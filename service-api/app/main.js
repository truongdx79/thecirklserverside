const express = require("express");
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var configs = require("../config");
var userRouter = require('../app/routers/user');
var interestRouter = require('../app/routers/interest');
var eventRouter = require('../app/routers/event');
var mailRouter = require('../app/routers/mail');
var jobFunctionRouter = require('../app/routers/job-function');
var jobRouter = require('../app/routers/job');
var skillRouter = require('../app/routers/skill');
var industryRouter = require('../app/routers/industry');
var readFileRouter = require('../app/routers/read-file');

const app = express();
const port = configs.port;

//Thiết lập một kết nối mongoose mặc định
mongoose.connect(`${configs.DNSRootServe}/${configs.databaseName}`, { useNewUrlParser: true, autoIndex: false });
//Ép Mongoose sử dụng thư viện promise toàn cục
mongoose.Promise = global.Promise;
//Lấy kết nối mặc định
var db = mongoose.connection;
// db.on('connected', function(){
//     console.log(connected("Mongoose default connection is open to "));
// });
//Ràng buộc kết nối với sự kiện lỗi (để lấy ra thông báo khi có lỗi)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// Reconnect when closed
// db.on('disconnected', function(){
//     console.log(disconnected("Mongoose default connection is disconnected"));
// });
app.use('/file', express.static('assets/'));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.use(userRouter);
app.use(interestRouter);
app.use(eventRouter);
app.use(mailRouter);
app.use(jobFunctionRouter);
app.use(jobRouter);
app.use(skillRouter);
app.use(industryRouter);

app.use(readFileRouter);

app.use(function (err, req, res, next) {
    if (!err.status) {
        res.status(400).json(
            {
                data: {},
                message: err.message,
                error: err
            }
        );
    } else {
        let mess = "";
        if (err.errors.length > 0) {
            err.errors.forEach(element => {
                mess = `${element.field[0]}${element.messages[0].split('\"')[element.messages[0].split('\"').length - 1]}`;
            });
        } else {
            mess = err;
        }
        res.status(err.status).json(
            {
                data: {},
                message: mess,
                error: err
            }
        );

    }
});

//start server rest API Nodejs
app.listen(port, function () {
    console.log(`Nodejs server started on port ${port}`)
});