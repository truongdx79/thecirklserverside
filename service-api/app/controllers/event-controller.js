const mongoose = require('mongoose');
const moment = require('moment');
const eventService = require('../services/event-service');
const userService = require('../services/user-service');
var Event = require('../models/event-model');
const objectId = mongoose.Types.ObjectId;

module.exports = {
    insert: insert,
    update: update,
    updateJoinMe: updateJoinMe,
    updateUnjoin: updateUnjoin,
    deleted: deleted,
    getAll: getAll,
    getAllRelated: getAllRelated,
    filter: filter,
    filterDate: filterDate,
    getDetail: getDetail,
    getAllJoinMe: getAllJoinMe,
    getAllMe: getAllMe
};

var callbackData = { data: {}, message: "", error: "" };

async function insert(userId, body, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "User Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(body.activity._id)) {
        callbackData.message = "Activity Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(userId) };
    let user = await userService.fildUserById(query);
    if (user == null) {
        callbackData.message = "The User does not exist";
        return callback(callbackData);
    }
    let restrictions = [];
    let checkObjectId = true;
    if (body.restrictions.length > 0) {
        body.restrictions.forEach(school => {
            if (!objectId.isValid(school._id)) {
                return checkObjectId = false;
            }
            restrictions.push({ _id: mongoose.Types.ObjectId(school._id), name: String(school.name) });
        });
    }
    if (!checkObjectId) {
        callbackData.message = "School Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    let fromDate = new Date(body.fromDate);
    let toDate = new Date(body.toDate);
    let fromTime = new Date(body.fromTime);
    let toTime = new Date(body.toTime);

    var offset = toDate.getTime() - fromDate.getTime();
    var days = Math.round(offset / 1000 / 60 / 60 / 24);
    let totalDay = "";
    if (days == 0) {
        let hours = toTime.getHours() - fromTime.getHours();
        totalDay = `${hours} Hours`;
    } else {
        totalDay = `${days} Days`;
    }
    const event = new Event({
        "name": String(body.name),
        "picture": String(body.picture),
        "location": String(body.location),
        "lat": Number(body.lat),
        "long": Number(body.long),
        "fromDate": fromDate,
        "toDate": toDate,
        "fromTime": body.fromTime,
        "toTime": body.toTime,
        "totalDay": totalDay,
        "description": String(body.description),
        // "activity": {
        //     "_id": mongoose.Types.ObjectId(body.activity._id),
        //     "name": body.activity.name
        // },
        // "owner": {
        //     "_id": mongoose.Types.ObjectId(user._id),
        //     "name": `${user.firstName} ${user.lastName}`,
        //     "avatarUrl": user.avatarUrl,
        //     "headline": user.headline
        // },
        "activity": mongoose.Types.ObjectId(body.activity._id),
        "owner": mongoose.Types.ObjectId(userId),
        "participate": [],
        "limitNumber": Number(body.limitNumber),
        "restrictions": restrictions,
        "_createDate": Date.now(),
        "_updateDate": Date.now()
    });
    eventService.insert(event, await function (result) {
        // console.log(result);

        return callback(result);
    });
}

function update(model, callback) {
    if (!objectId.isValid(model._id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.activity._id)) {
        callbackData.message = "Activity Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    let restrictions = [];
    let checkObjectId = true;
    if (model.restrictions.length > 0) {
        model.restrictions.forEach(school => {
            if (!objectId.isValid(school._id)) {
                return checkObjectId = false;
            }
            restrictions.push({ _id: mongoose.Types.ObjectId(school._id), name: String(school.name) });
        });
    }
    if (!checkObjectId) {
        callbackData.message = "School Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    let fromDate = new Date(model.fromDate);
    let toDate = new Date(model.toDate);
    let fromTime = new Date(model.fromTime);
    let toTime = new Date(model.toTime);

    var offset = toDate.getTime() - fromDate.getTime();
    var days = Math.round(offset / 1000 / 60 / 60 / 24);
    let totalDay = "";
    if (days == 0) {
        let hours = toTime.getHours() - fromTime.getHours();
        totalDay = `${hours} Hours`;
    } else {
        totalDay = `${days} Days`;
    }
    let event = {
        $set: {
            "name": String(model.name),
            "picture": String(model.picture),
            "location": String(model.location),
            "lat": Number(model.lat),
            "long": Number(model.long),
            "fromDate": fromDate,
            "toDate": toDate,
            "fromTime": model.fromTime,
            "toTime": model.toTime,
            "totalDay": totalDay,
            "description": String(model.description),
            "activity": mongoose.Types.ObjectId(model.activity._id),
            "restrictions": restrictions,
            "limitNumber": Number(model.limitNumber),
            "_updateDate": Date.now()
        }
    };
    eventService.update(query, event, function (result) {
        return callback(result);
    });
}

function updateJoinMe(model, callback) {
    if (!objectId.isValid(model._id)) {
        callbackData.message = "Event id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.objectId)) {
        callbackData.message = "Participate id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    let event = {
        $push: {
            "participate": mongoose.Types.ObjectId(model.objectId)
        }
    };
    eventService.update(query, event, function (result) {
        return callback(result);
    });
}

function updateUnjoin(model, callback) {
    if (!objectId.isValid(model._id)) {
        callbackData.message = "Event id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.objectId)) {
        callbackData.message = "Participate id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    let event = {
        $pull: {
            "participate": mongoose.Types.ObjectId(model.objectId)
        }
    };
    eventService.update(query, event, function (result) {
        return callback(result);
    });
}

function deleted(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    eventService.deleted(query, function (result) {
        return callback(result);
    });
}

function getDetail(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    const projections = {};
    const populate = [
        { path: 'activity', select: { name: true } },
        { path: 'participate', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true, headline: true } }
    ];
    eventService.getDetail(query, projections, populate, function (result) {
        return callback(result);
    });
}

function getAll(callback) {

    const query = {};
    const sort = { _createDate: -1 };
    const projections = { fromTime: false, toTime: false, description: false, _updateDate: false };
    const populate = [
        { path: 'activity', select: { name: true } },
        { path: 'participate', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true, headline: true } }
    ];
    eventService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}

function getAllRelated(activityId, callback) {
    if (!objectId.isValid(activityId)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { 'activity': mongoose.Types.ObjectId(activityId) };
    const sort = { _createDate: -1 };
    const projections = { activity: true, participate: true, fromDate: true, totalDay: true, name: true, location: true };
    const populate = [
        { path: 'activity', select: { name: true } },
        { path: 'participate', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true, headline: true } }
    ];
    eventService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}

function getAllMe(userId, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { 'owner': mongoose.Types.ObjectId(userId) };
    const sort = { _createDate: -1 };
    const projections = { fromTime: false, toTime: false, description: false, _updateDate: false };
    const populate = [
        { path: 'activity', select: { name: true } },
        { path: 'participate', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true, headline: true } }
    ];
    eventService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}

function getAllJoinMe(userId, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { 'participate': mongoose.Types.ObjectId(userId) };
    const sort = { _createDate: -1 };
    const projections = { fromTime: false, toTime: false, description: false, _updateDate: false };
    const populate = [
        { path: 'activity', select: { name: true } },
        { path: 'participate', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true, headline: true } }
    ];
    eventService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}

function filter(body, callback) {
    let chekcActivityId = true;
    let chekcFromDate = true;
    let chekcToDate = true;

    if (body.activityId === "" || body.activityId === null || body.activityId === undefined) {
        chekcActivityId = true;
    } else {
        if (!objectId.isValid(body.activityId)) {
            callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
            return callback(callbackData);
        }
        chekcActivityId = false;
    }
    if (body.fromDate === "" || body.fromDate === null || body.fromDate === undefined) {
        chekcFromDate = true;
    } else {
        chekcFromDate = false;
    }
    if (body.toDate === "" || body.toDate === null || body.toDate === undefined) {
        chekcToDate = true;
    } else {
        chekcToDate = false;
    }
    var restrictions = [];
    if (body.restrictions.length > 0) {
        body.restrictions.forEach(restr => {
            restrictions.push(mongoose.Types.ObjectId(restr._id));
        });
    }
    let query = {};

    if (!chekcActivityId && restrictions.length > 0 && !chekcFromDate && !chekcToDate) {
        query = {
            'activity': mongoose.Types.ObjectId(body.activityId),
            'restrictions._id': { $in: restrictions },
            toDate: { $lte: new Date(body.toDate).toISOString() },
            fromDate: { $gte: new Date(body.fromDate).toISOString() }
        };
    } else if (!chekcActivityId && restrictions.length > 0 && chekcFromDate && chekcToDate) {
        query = {
            'activity': mongoose.Types.ObjectId(body.activityId),
            'restrictions._id': { $in: restrictions }
        };
    } else if (!chekcActivityId && !chekcFromDate && !chekcToDate && restrictions.length < 1) {
        query = {
            'activity': mongoose.Types.ObjectId(body.activityId),
            toDate: { $lte: new Date(body.toDate).toISOString() },
            fromDate: { $gte: new Date(body.fromDate).toISOString() }
        };
    } else if (chekcActivityId && restrictions.length > 0 && !chekcFromDate && !chekcToDate) {
        query = {
            'restrictions._id': { $in: restrictions },
            toDate: { $lte: new Date(body.toDate).toISOString() },
            fromDate: { $gte: new Date(body.fromDate).toISOString() }
        };
    } else if (!chekcActivityId && !chekcToDate && restrictions.length < 1 && chekcFromDate) {
        query = {
            'activity': mongoose.Types.ObjectId(body.activityId),
            toDate: { $gte: new Date(body.toDate).toISOString() }
        };
    } else if (!chekcActivityId && !chekcFromDate && restrictions.length < 1 && chekcToDate) {
        query = {
            'activity': mongoose.Types.ObjectId(body.activityId),
            fromDate: { $gte: new Date(body.fromDate).toISOString() }
        };
    } else if (!chekcActivityId && restrictions.length < 1 && chekcFromDate && chekcToDate) {
        query = {
            'activity': mongoose.Types.ObjectId(body.activityId),
        };
    } else if (chekcActivityId && restrictions.length > 0 && !chekcFromDate && chekcToDate) {
        query = {
            'restrictions._id': { $in: restrictions },
            fromDate: { $gte: new Date(body.fromDate).toISOString() }
        };
    } else if (chekcActivityId && restrictions.length > 0 && chekcFromDate && !chekcToDate) {
        query = {
            'restrictions._id': { $in: restrictions },
            toDate: { $gte: new Date(body.toDate).toISOString() }
        };
    } else if (restrictions.length > 0 && chekcActivityId && chekcFromDate && chekcToDate) {
        query = {
            'restrictions._id': { $in: restrictions },
        };
    } else if (!chekcFromDate && !chekcToDate && chekcActivityId && restrictions.length < 1) {
        query = {
            fromDate: { $gte: new Date(body.fromDate).toISOString() },
            toDate: { $lte: new Date(body.toDate).toISOString() }
        };
    } else if (!chekcFromDate && chekcActivityId && restrictions.length < 1 && chekcToDate) {
        query = {
            fromDate: { $gte: new Date(body.fromDate).toISOString() }
        };
    } else if (chekcFromDate && chekcActivityId && restrictions.length < 1 && !chekcToDate) {
        query = {
            toDate: { $gte: new Date(body.toDate).toISOString() }
        };
    } else {
        query = {};
    }

    const filter = query;
    const projections = {};
    const sort = { _createDate: -1 };
    const populate = [
        { path: 'activity', select: { name: true } },
        { path: 'participate', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true, headline: true } }
    ];
    eventService.getAll(filter, projections, sort, populate, function (result) {
        return callback(result);
    });
}

function filterDate(params, callback) {
    var toDay = moment(new Date(params.date)).daysInMonth();
    var month = new Date(params.date).getMonth() + 1;
    var year = new Date(params.date).getFullYear();
    if (month < 10) month = "0" + month;
    if (toDay < 10) toDay = "0" + toDay;

    var fromDate = year + "-" + month + "-" + 01 + ' 00:00:00 GMT+0700';
    var toDate = year + "-" + month + "-" + toDay + ' 23:59:00 GMT+0700';

    const query = {
        fromDate: {
            $gte: new Date(fromDate).toISOString(),
            $lte: new Date(toDate).toISOString()
        }
    };
    const projections = {};
    const sort = { fromDate: -1 };
    const populate = [
        { path: 'activity', select: { name: true } },
        { path: 'participate', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true, headline: true } }
    ];
    eventService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}