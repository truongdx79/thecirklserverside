const mongoose = require('mongoose');
const interestService = require('../services/interest-service');
var Interest = require('../models/interest-model');
const objectId = mongoose.Types.ObjectId;

module.exports = {
    insert: insert,
    update: update,
    deleted: deleted,
    getAll: getAll,
    getDetail: getDetail
};

var callbackData = { data: {}, message: "", error: "" };

function insert(body, file, callback) {
    if (file) {
        const interest = new Interest({
            "name": String(body.name),
            "icon": `icons/${file.filename}`
        });
        interestService.insert(interest, function (result) {
            return callback(result);
        });
    } else {
        callbackData.message = "Icon is not empty";
        return callback(callbackData);
    }
}

function update(model, callback) {

    if (!objectId.isValid(model._id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };

    let interest = {
        $set: {
            "name": String(model.name),
            "icon": String(model.icon),
            "_updateDate": Date.now()
        }
    };
    interestService.update(query, interest, function (result) {
        return callback(result);
    });
}

function deleted(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    interestService.deleted(query, function (result) {
        return callback(result);
    });
}

function getDetail(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    const projections = {
    };
    interestService.getDetail(query, projections, function (result) {
        return callback(result);
    });
}

async function getAll(callback) {
    const query = { name: { $ne: 'Others' } };
    const projections = { _updateDate: false };
    const sort = { name: 1 };

    const queryOther = { name: 'Others' };
    let other = await interestService.filterOne(queryOther);
    interestService.getAll(query, projections, sort, function (result) {
        let skills = [];
        if (result.data) {
            skills = result.data;
            if (other) {
                skills.push(other);
            }
            callbackData.data = { result: skills };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        } else {
            return callback(result);
        }

    });
}