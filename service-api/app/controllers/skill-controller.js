const mongoose = require('mongoose');
const skillService = require('../services/skill-service');
var Skill = require('../models/skill-model');
const objectId = mongoose.Types.ObjectId;

module.exports = {
    insert: insert,
    update: update,
    deleted: deleted,
    getAll: getAll,
    getDetail: getDetail
};

var callbackData = { data: {}, message: "", error: "" };

function insert(body, callback) {
    const skill = new Skill({
        "name": String(body.name),
        "_createDate": Date.now(),
        "_updateDate": Date.now()
    });
    skillService.insert(skill, function (result) {
        return callback(result);
    });
}

function update(model, callback) {

    if (!objectId.isValid(model._id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };

    let skill = {
        $set: {
            "name": String(model.name),
            "_updateDate": Date.now()
        }
    };
    skillService.update(query, skill, function (result) {
        return callback(result);
    });
}

function deleted(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    skillService.deleted(query, function (result) {
        return callback(result);
    });
}

function getDetail(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    const projections = {

    };
    skillService.getDetail(query, projections, function (result) {
        return callback(result);
    });
}

function getAll(callback) {
    const query = {};
    const projections = { _createDate: false, _updateDate: false };
    const sort = { name: 0 };
    skillService.getAll(query, projections, sort, function (result) {
        return callback(result);
    });
}