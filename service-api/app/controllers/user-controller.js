const mongoose = require('mongoose');
const userService = require('../services/user-service');
var Users = require('../models/user-model');

const objectId = mongoose.Types.ObjectId;
var callbackData = { data: {}, message: "", error: "" };

module.exports = {
    login: login,
    adminLogin: adminLogin,
    checkExist: checkExist,
    insert: insert,
    update: update,
    updateAvailable: updateAvailable,
    updateLocation: updateLocation,
    updatePersonal: updatePersonal,
    updateCareer: updateCareer,
    updateEducation: updateEducation,
    updateActive: updateActive,
    deleted: deleted,
    getAll: getAll,
    getFullName: getFullName,
    getAllSchool: getAllSchool,
    getAllCompany: getAllCompany,
    getAllPosition: getAllPosition,
    getAllByAvailable: getAllByAvailable,
    getDetailById: getDetailById,
    getDetailByLinkedinId: getDetailByLinkedinId,
    getAllUserForMail: getAllUserForMail,
    filter: filter
};

function login(id, callback) {
    let query = { linkedinId: id }
    userService.login(query, function (result) {
        return callback(result);
    });
};

function adminLogin(model, callback) {
    let query = { emailAddress: String(model.email) }
    userService.adminLogin(query, model.password, function (result) {
        return callback(result);
    });
};

async function insert(model, callback) {
    const query = { linkedinId: model.linkedinId };

    let userExist = await userService.checkExistLinkedinId(query);

    if (userExist >= 1) {
        return callback({ data: {}, message: "LinkedinId already exists", error: "" });
    }

    let interests = [];
    let educations = [];
    let socials = [];
    let careers = [];
    let checkObjectId = true;
    let checkIndustry = true;


    if (model.interests.length > 0) {
        model.interests.forEach(interest => {
            if (objectId.isValid(interest._id)) {
                checkObjectId = true;
                interests.push(mongoose.Types.ObjectId(interest._id));
            } else {
                return checkObjectId = false;
            }
        });
    }
    if (!checkObjectId) {
        checkObjectId = true;
        return callback({ data: {}, message: "Interest Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }

    if (model.educations.length > 0) {
        model.educations.forEach(edu => {
            var eduId = "";
            if (objectId.isValid(edu._id)) {
                eduId = mongoose.Types.ObjectId(edu._id);
            } else {
                eduId = new mongoose.Types.ObjectId();
            }
            var obj = {
                _id: eduId,
                school: String(edu.school),
                degree: String(edu.degree),
                fieldOfStudy: String(edu.fieldOfStudy),
                graduationYear: String(edu.graduationYear)
            }
            educations.push(obj);
        });
    }

    if (model.careers.length > 0) {
        model.careers.forEach(ca => {
            if (objectId.isValid(ca.industryId)) {
                checkIndustry = true;
            } else {
                return checkIndustry = false;
            }

            let skills = [];
            if (ca.skills.length > 0) {
                ca.skills.forEach(sk => {
                    if (!objectId.isValid(sk._id)) {
                        return checkObjectId = false;
                    }
                    else {
                        checkObjectId = true;
                        skills.push(mongoose.Types.ObjectId(sk._id));
                    }
                });
            }
            careers.push({
                _id: new mongoose.Types.ObjectId(),
                company: String(ca.company),
                industry: mongoose.Types.ObjectId(ca.industryId),
                skills: skills,
                position: String(ca.position),
                level: String(ca.level),
                current: Boolean(ca.current),
                fromDate: ca.fromDate,
                toDate: ca.toDate
            });
        });
    }

    if (!checkIndustry) {
        return callback({ data: {}, message: "Industry Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    if (model.socials.length > 0) {
        model.socials.forEach(social => {
            if (social.name) {
                var obj = {
                    _id: new mongoose.Types.ObjectId(),
                    type: String(social.type),
                    name: String(social.name)
                }
                socials.push(obj);
            }
        });
    }

    const user = new Users({
        "linkedinId": String(model.linkedinId),
        "firstName": String(model.firstName),
        "lastName": String(model.lastName),
        "fullName": `${String(model.firstName)} ${String(model.lastName)}`,
        "company": String(model.company),
        "headline": String(model.headline),
        "avatarUrl": String(model.avatarUrl),
        "currentPosition": String(model.currentPosition),
        "location": String(model.location),
        "lat": Number(model.lat),
        "long": Number(model.long),
        "emailAddress": String(model.emailAddress),
        "phone": String(model.phone),
        "introduction": String(model.introduction),
        "interests": interests,
        "educations": educations,
        "available": Boolean(true),
        "careers": careers,
        "socials": socials,
    });
    userService.insert(user, await function (result) {
        return callback(result);
    });
};

async function checkExist(linkedinId, callback) {
    const query = { linkedinId: linkedinId };
    var existUser = await userService.checkExistLinkedinId(query);
    if (existUser == 1) {
        return callback({ data: true, message: "Success", error: "" });
    }
    else if (existUser == 0) {
        return callback({ data: false, message: "Success", error: "" });
    }
};

function update(model, callback) {
    let interests = [];
    let educations = [];
    let socials = [];
    let careers = [];
    let checkObjectId = true;
    let checkSkill = true;

    if (!objectId.isValid(model._id)) {
        return callback({ data: {}, message: "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    if (model.interests.length > 0) {
        model.interests.forEach(interest => {
            if (objectId.isValid(interest._id)) {
                checkObjectId = true;
                interests.push(mongoose.Types.ObjectId(interest._id));
            } else {
                return checkObjectId = false;
            }
        });
    }
    if (!checkObjectId) {
        return callback({ data: {}, message: "interest Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    if (model.educations.length > 0) {
        model.educations.forEach(edu => {
            var eduId = "";
            if (objectId.isValid(edu._id)) {
                eduId = mongoose.Types.ObjectId(edu._id);
            } else {
                eduId = new mongoose.Types.ObjectId();
            }
            var obj = {
                _id: eduId,
                school: String(edu.school),
                degree: String(edu.degree),
                fieldOfStudy: String(edu.fieldOfStudy),
                graduationYear: String(edu.graduationYear)
            }
            educations.push(obj);
        });
    }
    if (model.careers.length > 0) {
        model.careers.forEach(ca => {
            if (objectId.isValid(ca.industryId)) {
                checkIndustry = true;
            } else {
                return checkIndustry = false;
            }
            let skills = [];

            if (ca.skills.length > 0) {
                ca.skills.forEach(sk => {
                    if (!objectId.isValid(sk._id)) {
                        return checkSkill = false;
                    }
                    else {
                        checkSkill = true;
                        skills.push(mongoose.Types.ObjectId(sk._id));
                    }
                });
            }
            careers.push({
                _id: new mongoose.Types.ObjectId(),
                company: String(ca.company),
                industry: mongoose.Types.ObjectId(ca.industryId),
                skills: skills,
                position: String(ca.position),
                level: String(ca.level),
                current: Boolean(ca.current),
                fromDate: ca.fromDate,
                toDate: ca.toDate
            });
        });
    }
    if (!checkIndustry) {
        return callback({ data: {}, message: "Industry Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    if (!checkSkill) {
        return callback({ data: {}, message: "Skill Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }

    if (model.socials.length > 0) {
        model.socials.forEach(social => {
            if (social.name) {
                var obj = {
                    _id: new mongoose.Types.ObjectId(),
                    type: String(social.type),
                    name: String(social.name)
                }
                socials.push(obj);
            }
        });
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    const user = {
        $set: {
            "firstName": String(model.firstName),
            "lastName": String(model.lastName),
            "fullName": `${String(model.firstName)} ${String(model.lastName)}`,
            "company": String(model.company),
            "headline": String(model.headline),
            "avatarUrl": String(model.avatarUrl),
            "currentPosition": String(model.currentPosition),
            "location": String(model.location),
            "lat": Number(model.lat),
            "long": Number(model.long),
            "emailAddress": String(model.emailAddress),
            "phone": String(model.phone),
            "introduction": String(model.introduction),
            "interests": interests,
            "educations": educations,
            "available": Boolean(model.available),
            "careers": careers,
            "socials": socials
        }
    };
    userService.update(query, user, function (result) {
        return callback(result);
    });
};

function updateAvailable(model, callback) {
    if (!objectId.isValid(model._id)) {
        return callback({ data: {}, message: "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    const user = {
        $set: {
            "available": Boolean(model.available)
        }
    };
    userService.update(query, user, function (result) {
        return callback(result);
    });
};

function updateActive(model, callback) {
    if (!objectId.isValid(model._id)) {
        return callback({ data: {}, message: "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    const user = {
        $set: {
            "isActive": Boolean(model.isActive)
        }
    };
    userService.update(query, user, function (result) {
        return callback(result);
    });
};

function updateLocation(model, callback) {
    if (!objectId.isValid(model._id)) {
        return callback({ data: {}, message: "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    const user = {
        $set: {
            "location": String(model.location),
            "lat": Number(model.lat),
            "long": Number(model.long)
        }
    };
    userService.update(query, user, function (result) {
        return callback(result);
    });
};

function updatePersonal(model, callback) {
    if (!objectId.isValid(model._id)) {
        return callback({ data: {}, message: "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }

    let interests = [];
    let socials = [];
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };

    if (model.interests.length > 0) {
        model.interests.forEach(interest => {
            if (objectId.isValid(interest._id)) {
                checkObjectId = true;
                interests.push(mongoose.Types.ObjectId(interest._id));
            } else {
                return checkObjectId = false;
            }
        });
    }
    if (!checkObjectId) {
        return callback({ data: {}, message: "interest Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    if (model.socials.length > 0) {
        model.socials.forEach(social => {
            if (social.name) {
                socials.push({
                    _id: new mongoose.Types.ObjectId(),
                    type: String(social.type),
                    name: String(social.name)
                });
            }
        });
    }
    // model.industryId = "sdjfkdsj";
    const user = {
        $set: {
            lastName: String(model.lastName),
            firstName: String(model.firstName),
            fullName: `${String(model.firstName)} ${String(model.lastName)}`,
            headline: String(model.headline),
            currentPosition: String(model.currentPosition),
            company: String(model.company),
            location: String(model.location),
            lat: Number(model.lat),
            long: Number(model.long),
            emailAddress: String(model.emailAddress),
            phone: String(model.phone),
            introduction: String(model.introduction),
            socials: socials,
            interests: interests
        }
    };
    userService.update(query, user, function (result) {
        return callback(result);
    });
};

function updateEducation(model, callback) {
    if (!objectId.isValid(model._id)) {
        return callback({ data: {}, message: "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    let educations = [];

    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    if (model.educations.length > 0) {
        model.educations.forEach(edu => {
            var eduId = "";
            if (objectId.isValid(edu._id)) {
                eduId = mongoose.Types.ObjectId(edu._id);
            } else {
                eduId = new mongoose.Types.ObjectId();
            }
            var obj = {
                _id: eduId,
                school: String(edu.school),
                degree: String(edu.degree),
                fieldOfStudy: String(edu.fieldOfStudy),
                graduationYear: String(edu.graduationYear)
            }
            educations.push(obj);
        });
    }
    const user = {
        $set: {
            educations: educations
        }
    };
    userService.update(query, user, function (result) {
        return callback(result);
    });
};

function updateCareer(model, callback) {
    if (!objectId.isValid(model._id)) {
        return callback({ data: {}, message: "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    let careers = [];
    let checkIndustry = true;
    let checkSkill = true;
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };

    if (model.careers.length > 0) {
        model.careers.forEach(ca => {
            if (objectId.isValid(ca.industryId)) {
                checkIndustry = true;
            } else {
                return checkIndustry = false;
            }

            let skills = [];

            if (ca.skills.length > 0) {
                ca.skills.forEach(sk => {
                    if (!objectId.isValid(sk._id)) {
                        return checkSkill = false;
                    }
                    else {
                        checkSkill = true;
                        skills.push(mongoose.Types.ObjectId(sk._id));
                    }
                });
            }
            careers.push({
                _id: new mongoose.Types.ObjectId(),
                company: String(ca.company),
                industry: mongoose.Types.ObjectId(ca.industryId),
                skills: skills,
                position: String(ca.position),
                level: String(ca.level),
                current: Boolean(ca.current),
                fromDate: ca.fromDate,
                toDate: ca.toDate
            });
        });
    }
    if (!checkIndustry) {
        return callback({ data: {}, message: "Industry Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    if (!checkSkill) {
        return callback({ data: {}, message: "Skill Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    const user = {
        $set: {
            careers: careers
        }
    };
    userService.update(query, user, function (result) {
        return callback(result);
    });
};

function deleted(id, callback) {
    let query = { linkedinId: id };
    userService.deleted(query, function (result) {
        return callback(result);
    });
};

function getDetailById(id, callback) {
    if (!objectId.isValid(id)) {
        return callback({ data: {}, message: "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    let query = { _id: mongoose.Types.ObjectId(id) };
    let projections = {
        password: false, isActive: false,
        fullName: false
    };
    populate = [
        { path: 'interests', select: { name: true } },
        { path: 'careers.industry', select: { name: true } },
        { path: 'careers.skills', select: { name: true } }
    ];
    userService.getDetail(query, projections, populate, function (result) {
        return callback(result);
    });
};

function getDetailByLinkedinId(id, callback) {
    let query = { linkedinId: id };
    let projections = {
        password: false, isActive: false, fullName: false
    };
    populate = [
        { path: 'interests', select: { name: true } },
        { path: 'careers.industry', select: { name: true } },
        { path: 'careers.skills', select: { name: true } }
    ]
    userService.getDetail(query, projections, populate, function (result) {
        return callback(result);
    });
};

function getAll(id, callback) {
    let query = { _id: { "$nin": [mongoose.Types.ObjectId(id)] } };
    let projections = { password: false, fullName: false };
    let sorts = {};
    populate = [
        { path: 'interests', select: { name: true } },
        { path: 'careers.industry', select: { name: true } },
        { path: 'careers.skills', select: { name: true } }
    ];
    userService.getAll(query, projections, sorts, populate, function (result) {
        return callback(result);
    });
};

function getAllSchool(callback) {
    let query = [
        { $unwind: '$educations' },
        {
            $project: {
                _id: '$educations._id',
                name: '$educations.school'
            }
        }
    ];
    userService.findByAggregate(query, function (result) {
        const schools = [
            {
                "_id": "5c93a93c0c968d4bc519cb17",
                "name": "Kellogg-HKUST"
            },
            {
                "_id": "5c93a93c0c968d4bc519cb18",
                "name": "Kellogg"
            },
            {
                "_id": "5c944a9e0c968d4bc519cb19",
                "name": "Kellogg-Schulich"
            },
            {
                "_id": "5c944a9e0c968d4bc519cb20",
                "name": "Kellogg-Recanati"
            },
            {
                "_id": "5c944a9e0c968d4bc519cb21",
                "name": "Kellogg-WHU"
            },
            {
                "_id": "5c944a9e0c968d4bc519cb22",
                "name": "Kellogg-Guanghua"
            }
        ];
        if (result.message === 'Success') {
            var resultSchools = result.data.result.concat(schools);
            const uniqueValues = Array.from(new Set(resultSchools.map(s => s.name))).map(
                name => {
                    return {
                        _id: resultSchools.find(s => s.name === name)._id,
                        name: name
                    }
                }
            );
            uniqueValues.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
            callbackData.data = { result: uniqueValues };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        } else {
            return callback(result);
        }

    });
};

function getAllCompany(callback) {
    const field = "careers.company";
    const query = {};
    userService.findByDistinct(field, query, function (result) {
        return callback(result);
    });
};

function getAllPosition(callback) {
    const field = "careers.position";
    const query = {};
    userService.findByDistinct(field, query, function (result) {
        return callback(result);
    });
};

function getFullName(callback) {
    const field = "fullName";
    const query = {};
    userService.findByDistinct(field, query, function (result) {
        return callback(result);
    });
};


function getAllByAvailable(linkedinId, callback) {
    let query = { available: true, linkedinId: { "$nin": [linkedinId] } };
    let projections = { available: true, location: true, lat: true, long: true };

    let sorts = {
        _createDate: -1
    };
    populate = [];
    userService.getAll(query, projections, sorts, populate, function (result) {
        return callback(result);
    });
};

function getAllUserForMail(linkedinId, callback) {

    let query = { linkedinId: { "$nin": [linkedinId] } };
    let projections = { _id: true, available: true, firstName: true, lastName: true, avatarUrl: true };

    let sorts = {
        _createDate: -1
    };
    populate = [
    ];
    userService.getAll(query, projections, sorts, populate, function (result) {
        return callback(result);
    });
};

function filter(body, callback) {

    let checkPeople = false;
    let checkSchool = false;
    let checkCompany = false;
    let checkIndustry = false;
    let checkSkill = false;
    let checkLevel = false;
    let checkPosition = false;

    if (body.people === "" || body.people === null || body.people === undefined) {
        checkPeople = true;
    } else {
        checkPeople = false;
    }

    var interesting = [];
    if (body.interest.length > 0) {
        body.interest.forEach(i => {
            interesting.push(mongoose.Types.ObjectId(i._id));
        });
    }

    if (body.school === "" || body.school === null || body.school === undefined) {
        checkSchool = true;
    } else {
        checkSchool = false;
    }
    if (body.company === "" || body.company === null || body.company === undefined) {
        checkCompany = true;
    } else {
        checkCompany = false;
    }
    if (body.industry === "" || body.industry === null || body.industry === undefined) {
        checkIndustry = true;
    } else {
        if (!objectId.isValid(body.industry)) {
            callbackData.data = [];
            callbackData.message = "Industry Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
            return callback(callbackData);
        }
        checkIndustry = false;
    }

    var Skills = [];
    if (body.skills.length > 0) {
        body.skills.forEach(i => {
            Skills.push(mongoose.Types.ObjectId(i._id));
        });
    }
    if (body.level === "" || body.level === null || body.level === undefined) {
        checkLevel = true;
    } else {
        checkLevel = false;
    }
    if (body.position === "" || body.position === null || body.position === undefined) {
        checkPosition = true;
    } else {
        checkPosition = false;
    }


    let query = {};

    if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills }
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school)
        };
    } else if (!checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting }
        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i')
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting }
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school)
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.company": String(body.company),
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            // "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length > 0 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
        };
    } else if (!checkPeople && interesting.length > 0 && checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "interests": { $in: interesting },
            "careers.industry": mongoose.Types.ObjectId(body.industry),
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.level": String(body.level),
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && !checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.company": String(body.company),
            "educations.school": String(body.school),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "educations.school": String(body.school),
            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.company": String(body.company),
            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.company": String(body.company),

            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length > 0 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "educations.school": String(body.school),
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },

            "careers.skills": { $in: Skills },
        };
    } else if (checkPeople && interesting.length > 0 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },

        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            // "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.skills": { $in: Skills },
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.skills": { $in: Skills },
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.skills": { $in: Skills },
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.skills": { $in: Skills },
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),
            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && !checkCompany && checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.company": String(body.company),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),

        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.skills": { $in: Skills },
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),

            "careers.level": String(body.level)
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),

            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.level": String(body.level)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (checkPeople && interesting.length < 1 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "educations.school": String(body.school),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length > 0 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (!checkPeople && interesting.length < 1 && checkSchool && !checkCompany && !checkIndustry && Skills.length < 1 && !checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "fullName": new RegExp(String(body.people), 'i'),
            "careers.company": String(body.company),
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.level": String(body.level),
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length > 0 && !checkSchool && checkCompany && !checkIndustry && Skills.length < 1 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "interests": { $in: interesting },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && checkLevel && !checkPosition) {
        query = {

            "careers.skills": { $in: Skills },
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && !checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.industry": mongoose.Types.ObjectId(body.industry),

            "careers.skills": { $in: Skills },
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },

            "careers.skills": { $in: Skills },
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.skills": { $in: Skills },
            "careers.position": String(body.position)
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length > 0 && checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.skills": { $in: Skills }
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && !checkLevel && checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.level": String(body.level),
        };
    } else if (checkPeople && interesting.length < 1 && checkSchool && checkCompany && checkIndustry && Skills.length < 1 && checkLevel && !checkPosition) {
        query = {
            "role": { "$nin": ["system"] },
            "careers.position": String(body.position)
        };
    } else {
        query = { role: { "$nin": ["system"] } };
    }
    const filter = query;
    const projections = { isTrash: false, fullName: false };
    const sort = {};
    const populate = [
        { path: 'category', select: { name: true } },
        { path: 'accept', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true } }
    ];
    userService.getAll(filter, projections, sort, populate, function (result) {
        return callback(result);
    });
}

