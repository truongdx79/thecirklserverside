const mongoose = require('mongoose');
const jobFunctionService = require('../services/job-function-service');
var JobFunction = require('../models/job-function-model');
const objectId = mongoose.Types.ObjectId;

module.exports = {
    insert: insert,
    update: update,
    deleted: deleted,
    getAll: getAll,
    getDetail: getDetail
};

var callbackData = { data: {}, message: "", error: "" };

function insert(body, callback) {
    const jobFunction = new JobFunction({
        "name": String(model.name),
        "_createDate": Date.now(),
        "_updateDate": Date.now()
    });
    jobFunctionService.insert(jobFunction, function (result) {
        return callback(result);
    });
}

function update(model, callback) {

    if (!objectId.isValid(model._id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };

    let jobFunction = {
        $set: {
            "name": String(model.name),
            "_updateDate": Date.now()
        }
    };
    jobFunctionService.update(query, jobFunction, function (result) {
        return callback(result);
    });
}

function deleted(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    jobFunctionService.deleted(query, function (result) {
        return callback(result);
    });
}

function getDetail(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    const projections = {

    };
    jobFunctionService.getDetail(query, projections, function (result) {
        return callback(result);
    });
}

function getAll(callback) {
    const query = {};
    const projections = { _createDate: false, _updateDate: false };
    const sort = { name: 0 };
    jobFunctionService.getAll(query, projections, sort, function (result) {
        return callback(result);
    });
}