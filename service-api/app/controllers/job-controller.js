const mongoose = require('mongoose');
const jobService = require('../services/job-service');
const userService = require('../services/user-service');
var Job = require('../models/job-model');
const objectId = mongoose.Types.ObjectId;

module.exports = {
    insert: insert,
    update: update,
    userAccept: userAccept,
    updateWithdraw: updateWithdraw,
    updateRead: updateRead,
    deleted: deleted,
    getDetail: getDetail,
    getAll: getAll,
    getAllMe: getAllMe,
    getAllMeAccept: getAllMeAccept,
    filter: filter
};

var callbackData = { data: {}, message: "", error: "" };

async function insert(model, userId, callback) {
    if (!objectId.isValid(model.industry._id)) {
        callbackData.message = "Industry Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(userId) };
    let user = await userService.fildUserById(query);
    if (user == null) {
        callbackData.message = "The User does not exist";
        return callback(callbackData);
    }

    let skills = [];
    let checkObjectId = true;
    if (model.skills.length > 0) {
        model.skills.forEach(s => {
            if (!objectId.isValid(s._id)) {
                return checkObjectId = false;
            }
            skills.push(mongoose.Types.ObjectId(s._id));
        });
    }

    if (!checkObjectId) {
        callbackData.message = "Skill Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }

    let fromDate = new Date();
    let toDate = new Date(model.expirationDate);

    var offset = toDate.getTime() - fromDate.getTime();
    var days = Math.round(offset / 1000 / 60 / 60 / 24);
    let totalDay = "";
    if (days < 0) {
        callbackData.message = "The expiration date must be greater than the current date.";
        return callback(callbackData);
    } else {
        totalDay = `${days} Days`;
    }

    const job = new Job({
        classification: String(model.classification),
        jobType: String(model.jobType),
        title: String(model.title),
        location: String(model.location),
        long: Number(model.long),
        lat: Number(model.lat),
        industry: mongoose.Types.ObjectId(model.industry._id),
        skills: skills,
        experience: String(model.experience),
        salary: String(model.salary),
        currency: String(model.currency),
        salaryType: String(model.salaryType),
        startDate: Date.now(),
        expirationDate: model.expirationDate,
        totalDay: totalDay,
        description: String(model.description),
        isTrash: false,
        accept: [],
        owner: mongoose.Types.ObjectId(user._id),
        _createDate: Date.now(),
        _updateDate: Date.now()
    });
    jobService.insert(job, function (result) {
        // console.log(result);
        return callback(result);
    });
}
async function update(model, userId, callback) {

    if (!objectId.isValid(model._id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.industry._id)) {
        return callback({ data: {}, message: "Industry Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters", error: "" });
    }
    const queryUser = { _id: mongoose.Types.ObjectId(userId) };
    let user = await userService.fildUserById(queryUser);
    if (user == null) {
        callbackData.message = "The User does not exist";
        return callback(callbackData);
    }
    let skills = [];
    let checkObjectId = true;
    if (model.skills.length > 0) {
        model.skills.forEach(s => {
            if (!objectId.isValid(s._id)) {
                return checkObjectId = false;
            }
            skills.push(mongoose.Types.ObjectId(s._id));
        });
    }

    if (!checkObjectId) {
        callbackData.message = "Skill Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }

    let fromDate = new Date(model.startDate);
    let toDate = new Date(model.expirationDate);

    var offset = toDate.getTime() - fromDate.getTime();
    var days = Math.round(offset / 1000 / 60 / 60 / 24);
    let totalDay = "";
    if (days < 0) {
        callbackData.message = "The expiration date must be greater than the current date.";
        return callback(callbackData);
    } else {
        totalDay = `${days} Days`;
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    let job = {
        $set: {
            classification: String(model.classification),
            jobType: String(model.jobType),
            title: String(model.title),
            location: String(model.location),
            long: Number(model.long),
            lat: Number(model.lat),
            industry: mongoose.Types.ObjectId(model.industry._id),
            skills: skills,
            experience: String(model.experience),
            salary: String(model.salary),
            currency: String(model.currency),
            salaryType: String(model.salaryType),
            expirationDate: model.expirationDate,
            totalDay: totalDay,
            description: String(model.description),
            _updateDate: Date.now()
        }
    };
    jobService.update(query, job, function (result) {
        return callback(result);
    });
}
async function userAccept(model, callback) {

    if (!objectId.isValid(model._id)) {
        callbackData.message = "Jobs Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.accept._id)) {
        callbackData.message = "Accept Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const queryUser = { _id: mongoose.Types.ObjectId(model.accept._id) };
    let user = await userService.fildUserById(queryUser);
    if (user == null) {
        callbackData.message = "The User does not exist";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(model._id) };
    let job = {
        $push: {
            accept: mongoose.Types.ObjectId(model.accept._id)
        }
    };
    jobService.update(query, job, function (result) {
        return callback(result);
    });
}
function updateWithdraw(model, callback) {
    if (!objectId.isValid(model._id)) {
        callbackData.message = "Job id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.accept._id)) {
        callbackData.message = "Accept id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = {
        _id: mongoose.Types.ObjectId(model._id)
    };
    let job = {
        $pull: {
            accept: mongoose.Types.ObjectId(model.accept._id)
        }
    };
    jobService.update(query, job, function (result) {
        return callback(result);
    });
}
async function updateRead(model, callback) {
    if (!objectId.isValid(model._id)) {
        callbackData.message = "Job Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.read)) {
        callbackData.message = "Read Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(model._id) };
    let checkRead = await jobService.findOneRecord({ _id: mongoose.Types.ObjectId(model._id), 'read': model.read });
    if (checkRead) {
        callbackData.data = { length: 1 };
        callbackData.message = "Success";
        return callback(callbackData);
    } else {
        let mail = {
            $push: { "read": model.read }
        };
        jobService.update(query, mail, function (result) {
            return callback(result);
        });
    }
}
function deleted(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    jobService.deleted(query, function (result) {
        return callback(result);
    });
}
function getDetail(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    const projections = {
        isTrash: false
    };
    const populate = [
        { path: 'industry', select: { name: true } },
        { path: 'accept', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'skills', select: { name: true } }
    ];
    jobService.getDetail(query, projections, populate, function (result) {
        if (result.data) {
            let expiredDate = false;
            let fromDate = new Date();
            let toDate = new Date(result.data.expirationDate);
            let offset = toDate.getTime() - fromDate.getTime();
            let days = Math.round(offset / 1000 / 60 / 60 / 24);
            if (days < 0) {
                expiredDate = true
            } else {
                expiredDate = false;
            }
            Object.assign(result.data, { expired: expiredDate });
        }
        return callback(result);
    });
}
function getAll(callback) {
    const query = {};
    const projections = { isTrash: false };
    const sort = { _id: -1 };
    const populate = [
        { path: 'industry', select: { name: true } },
        { path: 'accept', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'skills', select: { name: true } }
    ];
    jobService.getAll(query, projections, sort, populate, function (result) {
        let jobs = [];
        if (result.data) {
            result.data.forEach(job => {
                let expiredDate = false;
                let fromDate = new Date();
                let toDate = new Date(job.expirationDate);
                let offset = toDate.getTime() - fromDate.getTime();
                let days = Math.round(offset / 1000 / 60 / 60 / 24);
                if (days < 0) {
                    expiredDate = true
                } else {
                    expiredDate = false;
                }
                Object.assign(job, { expired: expiredDate });
                jobs.push(job);
            });
        }
        if (jobs.length > 0) {
            callbackData.message = "Success";
            callbackData.data = { result: jobs }
            return callback(callbackData);
        } else {
            return callback(result);
        }

    });
}
function getAllMe(userId, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters"
        return callback(callbackData);
    }
    const query = { 'owner': mongoose.Types.ObjectId(userId) };
    const sort = { _createDate: -1 };
    const projections = { isTrash: false };
    const populate = [
        { path: 'industry', select: { name: true } },
        { path: 'accept', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'skills', select: { name: true } }
    ];
    jobService.getAll(query, projections, sort, populate, function (result) {
        let jobs = [];
        if (result.data) {
            result.data.forEach(job => {
                let expiredDate = false;
                let fromDate = new Date();
                let toDate = new Date(job.expirationDate);
                let offset = toDate.getTime() - fromDate.getTime();
                let days = Math.round(offset / 1000 / 60 / 60 / 24);
                if (days < 0) {
                    expiredDate = true
                } else {
                    expiredDate = false;
                }
                Object.assign(job, { expired: expiredDate });
                jobs.push(job);
            });
        }
        if (jobs.length > 0) {
            callbackData.message = "Success";
            callbackData.data = { result: jobs }
            return callback(callbackData);
        } else {
            return callback(result);
        }
    });
}
function getAllMeAccept(userId, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters"
        return callback(callbackData);
    }
    const query = { accept: mongoose.Types.ObjectId(userId) };
    const sort = { _createDate: -1 };
    const projections = { isTrash: false };
    const populate = [
        { path: 'industry', select: { name: true } },
        { path: 'accept', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'skills', select: { name: true } }
    ];
    jobService.getAll(query, projections, sort, populate, function (result) {
        let jobs = [];
        if (result.data) {
            result.data.forEach(job => {
                let expiredDate = false;
                let fromDate = new Date();
                let toDate = new Date(job.expirationDate);
                let offset = toDate.getTime() - fromDate.getTime();
                let days = Math.round(offset / 1000 / 60 / 60 / 24);
                if (days < 0) {
                    expiredDate = true
                } else {
                    expiredDate = false;
                }
                Object.assign(job, { expired: expiredDate });
                jobs.push(job);
            });
        }
        if (jobs.length > 0) {
            callbackData.message = "Success";
            callbackData.data = { result: jobs }
            return callback(callbackData);
        } else {
            return callback(result);
        }
    });
}
function filter(body, callback) {

    let checkClassification = false;
    let checkIndustry = false;
    let checkSalary = false;
    let checkSalaryType = false;
    let checkCurrency = false;
    if (body.classification === "" || body.classification === null || body.classification === undefined) {
        checkClassification = true;
    } else {
        checkClassification = false;
    }
    if (body.industry._id === "" || body.industry._id === null || body.industry._id === undefined) {
        checkIndustry = true;
    } else {
        if (!objectId.isValid(body.industry._id)) {
            callbackData.message = "Industry Id argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
            return callback(callbackData);
        }
        checkIndustry = false;
    }
    if (body.salary === "" || body.salary === null || body.salary === undefined) {
        checkSalary = true;
    } else {
        checkSalary = false;
    }
    if (body.salaryType === "" || body.salaryType === null || body.salaryType === undefined) {
        checkSalaryType = true;
    } else {
        checkSalaryType = false;
    }
    if (body.currency === "" || body.currency === null || body.currency === undefined) {
        checkCurrency = true;
    } else {
        checkCurrency = false;
    }
    let query = {};
    if (!checkClassification && !checkIndustry && !checkSalary && !checkSalaryType && !checkCurrency) {
        query = {
            classification: String(body.classification),
            industry: mongoose.Types.ObjectId(body.industry._id),
            salary: String(body.salary),
            currency: String(body.currency),
            salaryType: String(body.salaryType)
        };
    } else if (!checkClassification && !checkSalary && !checkSalaryType && !checkCurrency && checkIndustry) {
        query = {
            classification: String(body.classification),
            currency: String(body.currency),
            salary: String(body.salary),
            salaryType: String(body.salaryType)
        };
    } else if (!checkClassification && !checkSalaryType && !checkCurrency && !checkIndustry && checkSalary) {
        query = {
            classification: String(body.classification),
            currency: String(body.currency),
            industry: mongoose.Types.ObjectId(body.industry._id),
            salaryType: String(body.salaryType)
        };
    } else if (!checkClassification && !checkSalaryType && !checkIndustry && checkCurrency && checkSalary) {
        query = {
            classification: String(body.classification),
            industry: mongoose.Types.ObjectId(body.industry._id),
            salaryType: String(body.salaryType)
        };
    } else if (!checkClassification && !checkSalaryType && !checkCurrency && checkIndustry && checkSalary) {
        query = {
            classification: String(body.classification),
            currency: String(body.currency),
            salaryType: String(body.salaryType)
        };
    } else if (!checkClassification && checkIndustry && checkSalaryType && checkCurrency && checkSalary) {
        query = {
            classification: String(body.classification)
        };
    } else if (!checkClassification && !checkIndustry && checkSalaryType && checkCurrency && checkSalary) {
        query = {
            classification: String(body.classification),
            industry: mongoose.Types.ObjectId(body.industry._id)
        };
    } else if (!checkClassification && !checkSalary && checkSalaryType && checkCurrency && checkIndustry) {
        query = {
            classification: String(body.classification),
            salary: String(body.salary)
        };
    } else if (!checkClassification && !checkCurrency && checkSalaryType && checkIndustry && checkSalary) {
        query = {
            classification: String(body.classification),
            currency: String(body.currency)
        };
    } else if (!checkClassification && !checkSalaryType && checkIndustry && checkCurrency && checkSalary) {
        query = {
            classification: String(body.classification),
            salaryType: String(body.salaryType)
        };
    } else if (!checkClassification && !checkCurrency && !checkSalary && checkSalaryType && checkIndustry) {
        query = {
            classification: String(body.classification),
            currency: String(body.currency),
            salary: String(body.salary)
        };
    } else if (!checkClassification && !checkSalaryType && !checkSalary && checkCurrency && checkIndustry) {
        query = {
            classification: String(body.classification),
            salaryType: String(body.salaryType),
            salary: String(body.salary)
        };
    } else if (!checkIndustry && checkSalary && checkSalaryType && checkCurrency && checkClassification) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id)
        };
    } else if (!checkIndustry && !checkSalary && checkSalaryType && checkCurrency && checkClassification) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            salary: String(body.salary)
        };
    } else if (!checkIndustry && !checkCurrency && checkSalary && checkSalaryType && checkClassification) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            currency: String(body.currency)
        };
    } else if (!checkIndustry && !checkSalaryType && checkCurrency && checkSalary && checkClassification) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            salaryType: String(body.salaryType)
        };
    } else if (!checkIndustry && !checkSalaryType && !checkCurrency && checkSalary && checkClassification) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            salaryType: String(body.salaryType),
            currency: String(body.currency)
        };
    } else if (!checkIndustry && !checkSalaryType && !checkCurrency && !checkSalary && checkClassification) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            salaryType: String(body.salaryType),
            salary: String(body.salary),
            currency: String(body.currency)
        };
    } else if (!checkIndustry && !checkSalaryType && !checkClassification && !checkSalary && checkCurrency) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            classification: String(body.classification),
            salaryType: String(body.salaryType),
            salary: String(body.salary)
        };
    } else if (!checkIndustry && !checkSalaryType && !checkClassification && !checkSalary && checkCurrency) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            classification: String(body.classification),
            salaryType: String(body.salaryType),
            salary: String(body.salary)
        };
    } else if (!checkIndustry && !checkSalary && !checkCurrency && !checkClassification && checkSalaryType) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            classification: String(body.classification),
            currency: String(body.currency),
            salary: String(body.salary)
        };
    } else if (!checkIndustry && !checkSalary && !checkClassification && checkCurrency && checkSalaryType) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            classification: String(body.classification),
            salary: String(body.salary)
        };
    } else if (!checkSalary && checkCurrency && checkClassification && checkIndustry && checkSalaryType) {
        query = {
            salary: String(body.salary)
        };
    } else if (!checkSalary && !checkIndustry && !checkCurrency && checkClassification && checkSalaryType) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            salary: String(body.salary),
            currency: String(body.currency)
        };
    } else if (!checkSalary && !checkIndustry && !checkSalaryType && checkCurrency && checkClassification) {
        query = {
            industry: mongoose.Types.ObjectId(body.industry._id),
            salary: String(body.salary),
            salaryType: String(body.salaryType)
        };
    } else if (!checkSalary && !checkCurrency && checkIndustry && checkSalaryType && checkClassification) {
        query = {
            salary: String(body.salary),
            currency: String(body.currency)
        };
    } else if (!checkCurrency && checkSalary && checkClassification && checkIndustry && checkSalaryType) {
        query = {
            currency: String(body.currency)
        };
    } else if (!checkCurrency && !checkSalaryType && checkSalary && checkClassification && checkIndustry) {
        query = {
            salaryType: String(body.salaryType),
            currency: String(body.currency)
        };
    } else if (!checkCurrency && !checkSalaryType && !checkSalary && checkClassification && checkIndustry) {
        query = {
            salaryType: String(body.salaryType),
            salary: String(body.salary),
            currency: String(body.currency)
        };
    } else if (!checkSalaryType && checkCurrency && checkSalary && checkClassification && checkIndustry) {
        query = {
            salaryType: String(body.salaryType)
        };
    } else {
        query = {};
    }
    const filter = query;
    const projections = { isTrash: false };
    const sort = {};
    const populate = [
        { path: 'industry', select: { name: true } },
        { path: 'accept', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'owner', select: { firstName: true, lastName: true, available: true, avatarUrl: true } }
    ];
    jobService.getAll(filter, projections, sort, populate, function (result) {
        let jobs = [];
        if (result.data) {
            result.data.forEach(job => {
                let expiredDate = false;
                let fromDate = new Date();
                let toDate = new Date(job.expirationDate);
                let offset = toDate.getTime() - fromDate.getTime();
                let days = Math.round(offset / 1000 / 60 / 60 / 24);
                if (days < 0) {
                    expiredDate = true
                } else {
                    expiredDate = false;
                }
                Object.assign(job, { expired: expiredDate });
                jobs.push(job);
            });
        }
        if (jobs.length > 0) {
            callbackData.message = "Success";
            callbackData.data = { result: jobs }
            return callback(callbackData);
        } else {
            return callback(result);
        }
    });
}
