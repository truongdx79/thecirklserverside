const mongoose = require('mongoose');
const mailService = require('../services/mail-service');
const userService = require('../services/user-service');
var Mail = require('../models/mail-model');
const objectId = mongoose.Types.ObjectId;

module.exports = {
    insert: insert,
    insertReply: insertReply,
    updateStatusRead: updateStatusRead,
    updateIsTrash: updateIsTrash,
    deleted: deleted,
    getAllInbox: getAllInbox,
    getAllByParent: getAllByParent,
    getAllSent: getAllSent,
    getAllTrash: getAllTrash,
    getDetail: getDetail
};

var callbackData = { data: {}, message: "", error: "" };

async function insert(userId, model, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "From User Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(userId) };
    let user = await userService.fildUserById(query);
    if (user == null) {
        callbackData.message = "The User from does not exist";
        return callback(callbackData);
    }
    let userTo = [];
    let checkUserTo = false;
    if (model.userTo.length > 0) {
        model.userTo.forEach(u => {
            if (!objectId.isValid(u)) {
                return checkUserTo = true;
            }
            userTo.push(u);
        });
    } else {
        callbackData.message = "User To is not allowed to be empty";
        return callback(callbackData);
    }
    if (checkUserTo) {
        callbackData.message = "To User Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }

    const mail = new Mail({
        "userFrom": mongoose.Types.ObjectId(user._id),
        "userTo": userTo,
        "parentId": mongoose.Types.ObjectId('000000000000000000000000'),
        "title": String(model.title),
        "description": String(model.description),
        "reply": false,
        "_createDate": Date.now(),
        "read": []
    });
    mailService.insert(mail, function (result) {
        // console.log(result);
        return callback(result);
    });
}

async function insertReply(userId, model, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "From User Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.parentId)) {
        callbackData.message = "Parent Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(userId) };
    let user = await userService.fildUserById(query);
    if (user == null) {
        callbackData.message = "The User from does not exist";
        return callback(callbackData);
    }
    let userTo = [];
    let checkUserTo = false;
    if (model.userTo.length > 0) {
        model.userTo.forEach(u => {
            if (!objectId.isValid(u)) {
                return checkUserTo = true;
            }
            userTo.push(u);
        });
    } else {
        callbackData.message = "User To is not allowed to be empty";
        return callback(callbackData);
    }
    if (checkUserTo) {
        callbackData.message = "User To Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }

    //check exits mail root
    let userRoot = await mailService.findOneRecord({ _id: mongoose.Types.ObjectId(model.parentId) });

    //if exits maill
    if (userRoot) {
        if (userRoot.read.length > 0) {
            //check exits user to read mail
            let checkExitsUser = [];
            userRoot.read.filter((v, i) => {
                let exitsUser = userTo.filter(x => x == v);
                if (exitsUser.length > 0) {
                    checkExitsUser.push(v);
                }
            });
            const mail = new Mail({
                "userFrom": mongoose.Types.ObjectId(user._id),
                "userTo": userTo,
                "parentId": mongoose.Types.ObjectId(model.parentId),
                "title": String(model.title),
                "description": String(model.description),
                "reply": false,
                "_createDate": Date.now(),
                "read": []
            });
            if (checkExitsUser.length > 0) {
                //if exits then pull data
                let pullData = {
                    $set: { reply: true },
                    $pullAll: { read: checkExitsUser }
                };
                mailService.update({ _id: mongoose.Types.ObjectId(model.parentId) }, pullData, function (resultChangeRead) {
                    if (resultChangeRead.data.length > 0) {
                        mailService.insert(mail, function (result) {
                            return callback(result);
                        });
                    } else {
                        return callback(resultChangeRead);
                    }
                });
            } else {
                let updateData = {
                    $set: { reply: true }
                }
                mailService.update({ _id: mongoose.Types.ObjectId(model.parentId) }, updateData, function (resultChangReply) {
                    if (resultChangReply.data.length > 0) {
                        mailService.insert(mail, function (result) {
                            return callback(result);
                        });
                    } else {
                        return callback(resultChangReply);
                    }
                });
            }
        } else {
            callbackData.message = "Failure";
            return callback(callbackData);
        }
    } else {
        callbackData.message = "The Mail Root from does not exist";
        return callback(callbackData);
    }
}

async function updateStatusRead(model, callback) {
    if (!objectId.isValid(model._id)) {
        callbackData.message = "Mail Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    if (!objectId.isValid(model.read)) {
        callbackData.message = "Read Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(model._id) };
    let checkRead = await mailService.findOneRecord({ _id: mongoose.Types.ObjectId(model._id), 'read': String(model.read) });
    if (checkRead) {
        callbackData.data = { length: 1 };
        callbackData.message = "Success";
        return callback(callbackData);
    } else {
        let mail = {
            $push: { "read": String(model.read) }
        };
        mailService.update(query, mail, function (result) {
            return callback(result);
        });
    }
}

function updateIsTrash(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Mail Id Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    let mail = {
        $set: { 'isTrash': true }
    };
    mailService.update(query, mail, function (result) {
        return callback(result);
    });
}

function deleted(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { _id: mongoose.Types.ObjectId(id) };
    mailService.deleted(query, function (result) {
        return callback(result);
    });
}

async function getAllInbox(userId, callback) {
    const queryUser = { _id: mongoose.Types.ObjectId(userId) };
    let user = await userService.fildUserById(queryUser);
    if (user == null) {
        callbackData.message = "The User from does not exist";
        return callback(callbackData);
    }
    const query = {
        $or: [
            { 'userTo': mongoose.Types.ObjectId(user._id), 'parentId': mongoose.Types.ObjectId('000000000000000000000000') },
            { 'userFrom': mongoose.Types.ObjectId(user._id), 'reply': true, 'parentId': mongoose.Types.ObjectId('000000000000000000000000') }
        ],
        'isTrash': false
    };
    const projections = { reply: false };
    const sort = { _id: -1 };
    const populate = [
        { path: 'userFrom', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'userTo', select: { firstName: true, lastName: true, available: true, avatarUrl: true } }
    ];
    mailService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}

async function getAllByParent(id, callback) {
    if (!objectId.isValid(id)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const projections = { reply: false };
    const sort = { _id: -1 };
    const populate = [
        { path: 'userFrom', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'userTo', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },];
    const query = {
        $or: [
            { _id: mongoose.Types.ObjectId(id) },
            { parentId: mongoose.Types.ObjectId(id) }
        ]
    };
    mailService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}

async function getAllSent(userId, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const queryUser = { _id: mongoose.Types.ObjectId(userId) };
    let user = await userService.fildUserById(queryUser);
    if (user == null) {
        callbackData.message = "The User from does not exist";
        return callback(callbackData);
    }
    const query = { 'userFrom': mongoose.Types.ObjectId(user._id), 'isTrash': false };
    const projections = { reply: false };
    const sort = { _createDate: -1 };
    const populate = [
        { path: 'userFrom', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'userTo', select: { firstName: true, lastName: true, available: true, avatarUrl: true } }];
    mailService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}

async function getAllTrash(userId, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const queryUser = { _id: mongoose.Types.ObjectId(userId) };
    let user = await userService.fildUserById(queryUser);
    if (user == null) {
        callbackData.message = "The User from does not exist";
        return callback(callbackData);
    }
    const query = { 'userFrom': mongoose.Types.ObjectId(user._id), 'isTrash': true };
    const projections = { reply: false };
    const sort = { _createDate: -1 };
    const populate = [{ path: 'userFrom', select: { firstName: true, lastName: true, available: true, avatarUrl: true } }, { path: 'userTo', select: { firstName: true, lastName: true, available: true, avatarUrl: true } }];
    mailService.getAll(query, projections, sort, populate, function (result) {
        return callback(result);
    });
}

function getDetail(userId, callback) {
    if (!objectId.isValid(userId)) {
        callbackData.message = "Argument passed in must be a single String of 12 bytes or a string of 24 hex characters";
        return callback(callbackData);
    }
    const query = { '_id': mongoose.Types.ObjectId(userId) };
    const projections = { reply: false };
    const sort = { _createDate: -1 };
    const populate = [
        { path: 'userFrom', select: { firstName: true, lastName: true, available: true, avatarUrl: true } },
        { path: 'userTo', select: { firstName: true, lastName: true, available: true, avatarUrl: true } }];
    mailService.getDetail(query, projections, populate, function (result) {
        return callback(result);
    });
}