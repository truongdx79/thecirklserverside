var Joi = require('joi');

module.exports.post = {
  body: {    
    name: Joi.string().regex(/^[a-zA-Z0-9 ]*$/).required(),
    _createDate: Joi.date().default(Date.now, 'time of creation')
  }
};

module.exports.put = {
  body: {
    name: Joi.string().regex(/^[a-zA-Z0-9 ]*$/).required(),
    _updateDate: Joi.date().default(Date.now, 'time of creation')
  }
};


module.exports.paramId = {
  params: {
    id: Joi.string().regex(/^[a-zA-Z0-9]*$/).required()
  }
};


module.exports.query = {
  query: {
    id: Joi.string().regex(/^[a-zA-Z0-9]*$/).required()
  }
};