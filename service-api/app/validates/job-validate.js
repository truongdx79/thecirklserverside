var Joi = require('joi');

module.exports.post = {
  body: {    
    classification: Joi.string().valid('Jobseeker', 'Recruiter'),
    jobType: Joi.string().required(),
    title: Joi.string().required(),
    location: Joi.string().required(),
    long: Joi.number().required(),
    lat: Joi.number().required(),
    category: Joi.string().required(),
    skill: Joi.array().items(Joi.string().required()),
    experience: Joi.string(),
    Salary: Joi.string(),
    currency: Joi.string(),
    salaryType: Joi.string(),
    expirationDate: Joi.string(),
    description: Joi.string().required()
  }
};

module.exports.put = {
  body: {
    jobType: Joi.string().required(),
    title: Joi.string().required(),
    location: Joi.string().required(),
    long: Joi.number().required(),
    lat: Joi.number().required(),
    category: Joi.string().required(),
    skill: Joi.array().items(Joi.string().required()),
    experience: Joi.string().required(),
    Salary: Joi.string().required(),
    currency: Joi.string().required(),
    salaryType: Joi.string().required(),
    expirationDate: Joi.string().required(),
    description: Joi.string().required()
  }
};

module.exports.paramId = {
  params: {
    id: Joi.string().regex(/^[a-zA-Z0-9]*$/).required()
  }
};

module.exports.query = {
  query: {
    id: Joi.string().regex(/^[a-zA-Z0-9]*$/).required()
  }
};