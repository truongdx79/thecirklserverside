const Joi = require('joi');
let strRegexVn = /^[a-zA-Z0-9 `'.,-_/ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]*$/;
let strRegexEn = /^[a-zA-Z0-9]*$/;
let strPhone = /^\(?\+?[0-9]{1,4}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}$/;

module.exports.login = {
  body: {
    linkedinId: Joi.string().required()
  }
};

module.exports.post = {
  body: {
    linkedinId: Joi.string().required(),
    firstName: Joi.string().regex(strRegexVn).required(),
    lastName: Joi.string().regex(strRegexVn).required(),
    company: Joi.string().allow(''),
    headline: Joi.string().allow(''),
    avatarUrl: Joi.string().allow(''),
    currentPosition: Joi.string().allow(''),
    location: Joi.string().allow(''),
    lat: Joi.number().default(0).allow(''),
    long: Joi.number().default(0).allow(''),
    available: Joi.boolean().default(true),
    emailAddress: Joi.string().email().lowercase().allow(''),
    phone: Joi.string().regex(strPhone).allow(''), //example: (+84) 098 383 0933 or (+84)0938383093 or (+084) 093 883 0943 or (08)0938383093 or +098 494 9499 or + 098 494 4949 or 098 494 4949 or 0984944949
    introduction: Joi.string().allow(''),
    // interests: Joi.array().items(Joi.string()),
    interests: Joi.array().items({
      _id: Joi.string(),
      name: Joi.string().regex(strRegexVn)
    }),
    educations: Joi.array().items({
      school: Joi.string().regex(strRegexVn).allow(''),
      degree: Joi.string().regex(strRegexVn).allow(''),
      fieldOfStudy: Joi.string().regex(strRegexVn).allow(''),
      graduationYear: Joi.string().regex(strRegexVn).allow('')
    }),
    socials: Joi.array().items({
      type: Joi.string().allow(''),
      name: Joi.string().allow('')
    }),
    careers: Joi.array().items({
      company: Joi.string().allow(''),
      categoryId: Joi.string().allow(''),
      // skills: Joi.array().items(Joi.string().allow('')),
      skills: Joi.array().items({
        _id: Joi.string(),
        name: Joi.string().regex(strRegexVn)
      }),
      level: Joi.string().allow(''),
      fromDate: Joi.date().allow(''),
      toDate: Joi.date().allow('')
    }),
    password: Joi.string().alphanum().default(""),
    _createDate: Joi.date().default(Date.now, 'time of creation'),
    _updateDate: Joi.date().default(Date.now, 'time of creation'),
  }
};

module.exports.put = {
  body: {
    firstName: Joi.string().regex(strRegexVn).required(),
    lastName: Joi.string().regex(strRegexVn).required(),
    company: Joi.string().allow(''),
    headline: Joi.string().allow(''),
    avatarUrl: Joi.string().allow(''),
    currentPosition: Joi.string().allow(''),
    location: Joi.string().allow(''),
    lat: Joi.number().allow(''),
    long: Joi.number().allow(''),
    available: Joi.boolean(),
    emailAddress: Joi.string().email().lowercase().allow(''),
    phone: Joi.string().regex(strPhone).allow(''),
    introduction: Joi.string().allow(''),
    interests: Joi.array().items({
      _id: Joi.string(),
      name: Joi.string().regex(strRegexVn)
    }),
    // interests: Joi.array().items(Joi.string()),
    careers: Joi.array().items({
      company: Joi.string().allow(''),
      categoryId: Joi.string().allow(''),
      // skills: Joi.array().items(Joi.string().allow('')),
      skills: Joi.array().items({
        _id: Joi.string(),
        name: Joi.string().regex(strRegexVn)
      }),
      level: Joi.string().allow(''),
      fromDate: Joi.date().allow(''),
      toDate: Joi.date().allow('')
    }),
    educations: Joi.array().items({
      school: Joi.string().regex(strRegexVn).allow(''),
      degree: Joi.string().regex(strRegexVn).allow(''),
      fieldOfStudy: Joi.string().regex(strRegexVn).allow(''),
      graduationYear: Joi.string().regex(strRegexVn).allow('')
    }),
    socials: Joi.array().items({
      type: Joi.string().allow(''),
      name: Joi.string().allow('')
    }),
    password: Joi.string().alphanum().default(""),
    _updateDate: Joi.date().default(Date.now, 'time of creation')
  }
};

module.exports.putAvailable = {
  body: {
    _id: Joi.string().required(),
    available: Joi.boolean().required()
  }
};

module.exports.putActive = {
  body: {
    _id: Joi.string().required(),
    isActive: Joi.boolean().required()
  }
};

module.exports.putLocation = {
  body: {
    _id: Joi.string().required(),
    location: Joi.string().required(),
    lat: Joi.number().required(),
    long: Joi.number().required(),
  }
};

module.exports.putEducation = {
  body: {
    _id: Joi.string().required(),
    educations: Joi.array().items({
      school: Joi.string().required(),
      degree: Joi.string().allow(''),
      fieldOfStudy: Joi.string().allow(''),
      graduationYear: Joi.string().allow('')
    })
  }
};

module.exports.putCareer = {
  body: {
    _id: Joi.string().required(),
    careers: Joi.array().items({
      company: Joi.string().allow(''),
      categoryId: Joi.string().allow(''),
      skills: Joi.array().items({
        _id: Joi.string(),
        name: Joi.string().regex(strRegexVn)
      }),
      level: Joi.string().allow(''),
      fromDate: Joi.date().allow(''),
      toDate: Joi.date().allow('')
    })
  }
};

module.exports.putPersonal = {
  body: {
    _id: Joi.string().required(),
    firstName: Joi.string().regex(strRegexVn).required(),
    lastName: Joi.string().regex(strRegexVn).required(),
    company: Joi.string().allow(''),
    headline: Joi.string().allow(''),
    currentPosition: Joi.string().allow(''),
    location: Joi.string().required(),
    lat: Joi.number().required(),
    long: Joi.number().required(),
    emailAddress: Joi.string().email().lowercase().allow(''),
    phone: Joi.string().regex(strPhone).allow(''),
    introduction: Joi.string().allow(''),
    // interests: Joi.array().items(Joi.string()),
    interests: Joi.array().items({
      _id: Joi.string(),
      name: Joi.string().regex(strRegexVn)
    }),
    socials: Joi.array().items({
      type: Joi.string().allow(''),
      name: Joi.string().allow('')
    })
  }
};

module.exports.paramLinkedin = {
  params: {
    linkedinId: Joi.string().required()
  }
};

module.exports.paramId = {
  params: {
    id: Joi.string().required()
  }
};

module.exports.query = {
  query: {
    linkedinId: Joi.string().required()
  }
};