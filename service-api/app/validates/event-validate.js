const Joi = require('joi');
let strRegexEn = /^[a-zA-Z0-9]*$/;

module.exports.login = {
  body: {
    linkedinId: Joi.string().required()
  }
};

module.exports.post = {
  body: {
    name: Joi.string().required(),
    location: Joi.string().required(),
    lat: Joi.number().required(),
    long: Joi.number().required(),
    picture: Joi.string().allow(''),
    fromDate: Joi.date().required(),
    toDate: Joi.date().required(),
    fromTime: Joi.date(),
    toTime: Joi.date(),
    description: Joi.string().allow(''),
    limitNumber: Joi.number().allow(''),
    activity: Joi.object({
      _id: Joi.string(),
      name: Joi.string()
    })
  }
};

module.exports.put = {
  body: {
    _id: Joi.string().required(),
    name: Joi.string().required(),
    location: Joi.string().required(),
    lat: Joi.number().required(),
    long: Joi.number().required(),
    picture: Joi.string().allow(''),
    fromDate: Joi.date().required(),
    toDate: Joi.date().required(),
    fromTime: Joi.date(),
    toTime: Joi.date(),
    description: Joi.string().allow(''),
    limitNumber: Joi.number().allow(''),
    activity: Joi.object({
      _id: Joi.string().required(),
      name: Joi.string().required()
    })
  }
};

module.exports.paramId = {
  params: {
    id: Joi.string().regex(strRegexEn).required()
  }
};

module.exports.paramFilter = {
  body: {
    activityId: Joi.string().regex(strRegexEn).allow(''),
    restrictions: Joi.array().items({
      _id: Joi.string().regex(strRegexEn).allow(''),
      school: Joi.string().allow('')
    }),
    fromDate: Joi.date().allow(''),
    toDate: Joi.date().allow('')
  }
};

module.exports.paramJoinMe = {
  body: {
    participate: Joi.array().items(
      { //nguoi tham gia event
        _id: Joi.string().required(), //id event
        objectId: Joi.string().required(), //id user join
        name: Joi.string().required(),
        avatarUrl: Joi.string().required()
      }
    )
  }
};
