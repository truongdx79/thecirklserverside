var jwt = require('jsonwebtoken');
var config = require('../../config');
function verifyToken(req, res, next) {
  var token = req.headers['authorization'];
  if (!token)
    return res.status(404).send({ data: { auth: false }, message: 'Not token provided.', error: "" });
  jwt.verify(token, config.secret, function (err, decoded) {
    if (err)
      return res.status(500).send({ data: { auth: false }, message: 'Failed to authenticate token.', error: err });
    // if everything good, save to request for use in other routes
    req.userId = decoded.linkedinId;
    req._id = decoded.id;
    next();
  });
}
module.exports = verifyToken;