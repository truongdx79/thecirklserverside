var router = require('express').Router();
const interestController = require('../controllers/interest-controller');
const multer = require('multer');
let DIR = 'assets/icons/';

let storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, DIR);
    },
    filename: function (req, file, cb) {
        let datetimestamp = Date.now();
        cb(null, datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    },
    fileFilter: function (req, file, cb) {
        // accept image only
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
            return cb(new Error('Only image files are allowed!'), false);
        }
        cb(null, true);
    }
});

let upload = multer({
    storage: storage
})

router.post('/api/interest/insert', upload.single('icon'), function (req, res, next) {
    const file = req.file;
    const body = req.body;
    interestController.insert(body, file, function (result) {
        res.send(result);
    });
});

router.put('/api/interest/update', function (req, res, next) {
    const interest = req.body;
    interestController.update(interest, function (result) {
        res.send(result);
    });
});

router.delete('/api/interest/delete/:id', function (req, res, next) {
    const id = req.params.id;
    interestController.deleted(id, function (result) {
        res.send(result);
    });
});

router.get('/api/interest', function (req, res, next) {
    interestController.getAll(function (result) {
        res.send(result);
    });
});

router.get('/api/interest/:id', function (req, res, next) {
    const id = req.params.id;
    interestController.getDetail(id, function (result) {
        res.send(result);
    });
});
module.exports = router;