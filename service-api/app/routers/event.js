const mongoose = require('mongoose');
var router = require('express').Router();
const multer = require('multer');

var validate = require('express-validation');
const verifyTokent = require('../common/verify-token');

const eventController = require('../controllers/event-controller');
const validateEvent = require('../validates/event-validate');

let DIR = 'assets/events/';

let storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, DIR);
    },
    filename: function (req, file, cb) {
        let datetimestamp = Date.now();
        cb(null, datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    },
    fileFilter: function (req, file, cb) {
        // accept image only
        if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
            return cb(new Error('Only image files are allowed!'), false);
        }
        cb(null, true);
    }
});

let upload = multer({ //multer settings
    storage: storage
});

/** API path that will upload the files */
router.post('/event/upload', upload.single('events'), function (req, res) {
    if (!req.file) {
        return res.send({
            success: false
        });
    } else {
        res.send(req.file);
    }
});
router.post('/api/event/insert', verifyTokent, validate(validateEvent.post), function (req, res, next) {
    const userId = req._id;
    eventController.insert(userId, req.body, function (result) {
        res.send(result);
    });
});
router.put('/api/event/update', verifyTokent, validate(validateEvent.put), function (req, res, next) {
    eventController.update(req.body, function (result) {
        res.send(result);
    });
});
router.put('/api/event/join', verifyTokent, validate(validateEvent.paramJoinMe), function (req, res, next) {
    eventController.updateJoinMe(req.body, function (result) {
        res.send(result);
    });
});
router.put('/api/event/withdraw', verifyTokent, validate(validateEvent.paramJoinMe), function (req, res, next) {
    eventController.updateUnjoin(req.body, function (result) {
        res.send(result);
    });
});
router.delete('/api/event/delete/:id', verifyTokent, validate(validateEvent.paramId), function (req, res, next) {
    const id = req.params.id;
    eventController.deleted(id, function (result) {
        res.send(result);
    });
});
router.get('/api/events', verifyTokent, function (req, res, next) {
    eventController.getAll(function (result) {
        res.send(result);
    });
});
router.get('/api/events/related/:id', verifyTokent, validate(validateEvent.paramId), function (req, res, next) {
    var id = req.params.id;
    eventController.getAllRelated(id, function (result) {
        res.send(result);
    });
});
router.post('/api/event/filter', verifyTokent, validate(validateEvent.paramFilter), function (req, res, next) {
    var param = req.body;
    eventController.filter(param, function (result) {
        res.send(result);
    });
});
router.get('/api/event/filter/date/:date', verifyTokent, function (req, res, next) {
    var param = req.params;
    eventController.filterDate(param, function (result) {
        res.send(result);
    });
});
router.get('/api/event/:id', verifyTokent, validate(validateEvent.paramId), function (req, res, next) {
    var id = req.params.id;
    eventController.getDetail(id, function (result) {
        res.send(result);
    });
});
router.get('/api/events/me', verifyTokent, function (req, res, next) {
    var id = req._id;
    eventController.getAllMe(id, function (result) {
        res.send(result);
    });
});
router.get('/api/events/join/me', verifyTokent, function (req, res, next) {
    var id = req._id;
    eventController.getAllJoinMe(id, function (result) {
        res.send(result);
    });
});

module.exports = router;