const express = require("express");
var router = require('express').Router();
// var path = require('path');
var fs = require('fs');
var url = require('url');

router.get('/api/icon', function (req, res) {
    var query = url.parse(req.url, true).query;
    var pic = query.pic;
    //read the image using fs and send the image content back in the response
    fs.readFile(`${process.cwd()}/assets/icons/${pic}`, function (err, content) {
        if (err) {
            res.writeHead(400, { 'Content-type': 'text/html' })
            res.end("No such image");
        } else {
            //specify the content type in the response will be an image
            res.writeHead(200, { 'Content-type': '*' });
            res.end(content);
        }
    });
});

router.get('/image', function (req, res) {
    var query = url.parse(req.url, true).query;
    var pic = query.image;
    if (pic === undefined) {
        getImages(`${process.cwd()}/assets/icon/`, function (err, result) {
            if (err) {
                return res.json({ status: false });
            }
            return res.send({ status: true, data: result });
        });
    } else {
        //read the image using fs and send the image content back in the response
        fs.readFile(`${process.cwd()}/assets/icon/${pic}`, function (err, content) {
            if (err) {
                res.writeHead(400, { 'Content-type': 'text/html' })
                res.end("No such image");
            } else {
                //specify the content type in the response will be an image
                res.writeHead(200, { 'Content-type': '*' });
                res.end(content);
            }
        });
    }
});
//get the list of files in the image dir
function getImages(dir, callback) {
    files = [];
    fs.readdir(dir, function (err, list) {
        for (var i = 0; i < list.length; i++) {
            files.push({ icon: list[i] });
        }
        callback(err, files);
    });
}

module.exports = router;