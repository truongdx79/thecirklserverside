var router = require('express').Router();
const skillController = require('../controllers/skill-controller');
const verifyTokent = require('../common/verify-token');

router.post('/api/skill/insert', function (req, res, next) {
    const body = req.body;
    skillController.insert(body, function (result) {
        res.send(result);
    });
});

router.put('/api/skill/update', verifyTokent, function (req, res, next) {
    const skill = req.body;
    skillController.update(skill, function (result) {
        res.send(result);
    });
});

router.delete('/api/skill/delete/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    skillController.deleted(id, function (result) {
        res.send(result);
    });
});

router.get('/api/skills', function (req, res, next) {
    skillController.getAll(function (result) {
        res.send(result);
    });
});

router.get('/api/skill/detail/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    skillController.getDetail(id, function (result) {
        res.send(result);
    });
});
module.exports = router;