var router = require('express').Router();
const jobController = require('../controllers/job-controller');
const verifyTokent = require('../common/verify-token');

router.post('/api/job/insert', verifyTokent, function (req, res, next) {
    const job = req.body;
    const userid = req._id;
    jobController.insert(job, userid, function (result) {
        res.send(result);
    });
});
router.put('/api/job/update', verifyTokent, function (req, res, next) {
    const job = req.body;
    const userid = req._id;

    jobController.update(job, userid, function (result) {
        res.send(result);
    });
});
router.put('/api/job/accept', verifyTokent, function (req, res, next) {
    const job = req.body;
    jobController.userAccept(job, function (result) {
        res.send(result);
    });
});
router.put('/api/job/withdraw', verifyTokent, function (req, res, next) {
    const job = req.body;
    jobController.updateWithdraw(job, function (result) {
        res.send(result);
    });
});
router.put('/api/job/read', verifyTokent, function (req, res, next) {
    const job = req.body;
    jobController.updateRead(job, function (result) {
        res.send(result);
    });
});
router.delete('/api/job/delete/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    jobController.deleted(id, function (result) {
        res.send(result);
    });
});
router.get('/api/jobs', verifyTokent, function (req, res, next) {
    jobController.getAll(function (result) {
        res.send(result);
    });
});
router.get('/api/jobs/me', verifyTokent, function (req, res, next) {
    const userid = req._id;
    jobController.getAllMe(userid, function (result) {
        res.send(result);
    });
});
router.get('/api/jobs/me/accept', verifyTokent, function (req, res, next) {
    const userid = req._id;
    jobController.getAllMeAccept(userid, function (result) {
        res.send(result);
    });
});
router.post('/api/job/filter', verifyTokent, function (req, res, next) {
    const search = req.body;
    jobController.filter(search, function (result) {
        res.send(result);
    });
});
router.get('/api/job/detail/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    jobController.getDetail(id, function (result) {
        res.send(result);
    });
});
module.exports = router;