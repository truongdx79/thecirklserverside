var router = require('express').Router();
const industryController = require('../controllers/industry-controller');
const verifyTokent = require('../common/verify-token');



router.post('/api/industry/insert', verifyTokent, function (req, res, next) {
    const industry = req.body;
    industryController.insert(industry, function (result) {
        res.send(result);
    });
});

router.put('/api/industry/update', verifyTokent, function (req, res, next) {
    const industry = req.body;
    industryController.update(industry, function (result) {
        res.send(result);
    });
});

router.delete('/api/industry/delete/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    industryController.deleted(id, function (result) {
        res.send(result);
    });
});

router.get('/api/industries', function (req, res, next) {
    industryController.getAll(function (result) {
        res.send(result);
    });
});

router.get('/api/industry/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    industryController.getDetail(id, function (result) {
        res.send(result);
    });
});
module.exports = router;