var router = require('express').Router();
var validate = require('express-validation');
const verifyTokent = require('../common/verify-token');

const mailController = require('../controllers/mail-controller');

router.post('/api/mail/insert', verifyTokent, function (req, res, next) {
    const userId = req._id;
    const body = req.body;
    mailController.insert(userId, body, function (result) {
        res.send(result);
    });
});
router.post('/api/mail/reply', verifyTokent, function (req, res, next) {
    const userId = req._id;
    const body = req.body;
    mailController.insertReply(userId, body, function (result) {
        res.send(result);
    });
});

router.put('/api/mail/read/update', verifyTokent, function (req, res, next) {
    const mail = req.body;
    mailController.updateStatusRead(mail, function (result) {
        res.send(result);
    });
});

router.delete('/api/mail/trash/:id', verifyTokent, function (req, res, next) {
    const mailId = req.params.id;
    mailController.updateIsTrash(mailId, function (result) {
        res.send(result);
    });
});

router.delete('/api/mail/delete/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    mailController.deleted(id, function (result) {
        res.send(result);
    });
});

router.get('/api/mails', verifyTokent, function (req, res, next) {
    const userId = req._id;
    mailController.getAllInbox(userId, function (result) {
        res.send(result);
    });
});

router.get('/api/mail/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    mailController.getAllByParent(id, function (result) {
        res.send(result);
    });
});

router.get('/api/mail/all/sent', verifyTokent, function (req, res, next) {
    const userId = req._id;
    mailController.getAllSent(userId, function (result) {
        res.send(result);
    });
});

router.get('/api/mail/all/trash', verifyTokent, function (req, res, next) {
    const userId = req._id;
    mailController.getAllTrash(userId, function (result) {
        res.send(result);
    });
});

router.get('/api/mail/detail/:id', verifyTokent, function (req, res, next) {
    const id = req.params.id;
    mailController.getDetail(id, function (result) {
        res.send(result);
    });
});

module.exports = router;