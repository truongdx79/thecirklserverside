var router = require('express').Router();
var validate = require('express-validation');
const verifyTokent = require('../common/verify-token');

const userController = require('../controllers/user-controller');
const validateUser = require('../validates/user-validate');

router.post('/login', validate(validateUser.login), function (req, res, next) {
    const id = req.body.linkedinId;
    userController.login(id, function (result) {
        res.send(result);
    });
});
router.post('/api/admin/login', function (req, res, next) {
    const user = req.body;
    userController.adminLogin(user, function (result) {
        res.send(result);
    });
});


router.get('/api/user/checkexist/:linkedinId', validate(validateUser.paramLinkedin), function (req, res, next) {
    const id = req.params.linkedinId;
    userController.checkExist(id, function (result) {
        res.send(result);
    });
});
router.post('/api/user/insert', function (req, res, next) {
    // const host = String(req.headers.host);
    userController.insert(req.body, function (result) {
        res.send(result);
    });
});

router.put('/api/user/update', verifyTokent, function (req, res, next) {
    const user = req.body;
    userController.update(user, function (result) {
        res.send(result);
    });
});

router.put('/api/user/location/update', verifyTokent, function (req, res, next) {
    const user = req.body;
    userController.updateLocation(user, function (result) {
        res.send(result);
    });
});
router.put('/api/user/education/update', verifyTokent, function (req, res, next) {
    const user = req.body;
    userController.updateEducation(user, function (result) {
        res.send(result);
    });
});
router.put('/api/user/career/update', verifyTokent, function (req, res, next) {
    const user = req.body;
    userController.updateCareer(user, function (result) {
        res.send(result);
    });
});
router.put('/api/user/personal/update', verifyTokent, function (req, res, next) {
    const user = req.body;
    userController.updatePersonal(user, function (result) {
        res.send(result);
    });
});
router.put('/api/user/available/update', verifyTokent, function (req, res, next) {
    const user = req.body;
    userController.updateAvailable(user, function (result) {
        res.send(result);
    });
});
router.put('/api/user/active', verifyTokent, function (req, res, next) {
    const user = req.body;
    userController.updateActive(user, function (result) {
        res.send(result);
    });
});
router.delete('/api/user/delete/:linkedinId', verifyTokent, validate(validateUser.paramLinkedin), function (req, res, next) {
    const id = req.params.linkedinId;
    userController.deleted(id, function (result) {
        res.send(result);
    });
});
router.get('/api/user/all', verifyTokent, function (req, res, next) {
    const id = req._id;
    userController.getAll(id, function (result) {
        res.send(result);
    });
});
router.get('/api/users', verifyTokent, function (req, res, next) {
    var linkedinId = req.userId;
    userController.getAllByAvailable(linkedinId, function (result) {
        res.send(result);
    });
});
router.get('/api/user/formail', verifyTokent, function (req, res, next) {
    var linkedinId = req.userId;
    userController.getAllUserForMail(linkedinId, function (result) {
        res.send(result);
    });
});
router.get('/api/user/profile', verifyTokent, function (req, res, next) {
    var id = req.userId;
    userController.getDetailByLinkedinId(id, function (result) {
        res.send(result);
    });
});
router.get('/api/user/detail/:id', verifyTokent, validate(validateUser.paramId), function (req, res, next) {
    var id = req.params.id;
    userController.getDetailById(id, function (result) {
        res.send(result);
    });
});
router.get('/api/user/fullname/all', verifyTokent, function (req, res, next) {
    userController.getFullName(function (result) {
        res.send(result);
    });
});
router.get('/api/user/schools/all', verifyTokent, function (req, res, next) {
    userController.getAllSchool(function (result) {
        res.send(result);
    });
});
router.get('/api/user/company/all', verifyTokent, function (req, res, next) {
    userController.getAllCompany(function (result) {
        res.send(result);
    });
});
router.get('/api/user/position/all', verifyTokent, function (req, res, next) {
    userController.getAllPosition(function (result) {
        res.send(result);
    });
});

router.post('/api/user/search', verifyTokent, function (req, res, next) {
    const user = req.body;
    userController.filter(user, function (result) {
        res.send(result);
    });
});

module.exports = router;