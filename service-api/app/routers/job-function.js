var router = require('express').Router();
const jobFunctionController = require('../controllers/job-function-controller');
const verifyTokent = require('../common/verify-token');



router.post('/api/category/insert',verifyTokent, function (req, res, next) {
    const jobFunction = req.body;
    jobFunctionController.insert(jobFunction, function (result) {
        res.send(result);
    });
});

router.put('/api/category/update',verifyTokent, function (req, res, next) {
    const jobFunction = req.body;
    jobFunctionController.update(jobFunction, function (result) {
        res.send(result);
    });
});

router.delete('/api/category/delete/:id',verifyTokent, function (req, res, next) {
    const id = req.params.id;
    jobFunctionController.deleted(id, function (result) {
        res.send(result);
    });
});

router.get('/api/categories', function (req, res, next) {
    jobFunctionController.getAll(function (result) {
        res.send(result);
    });
});

router.get('/api/category/:id',verifyTokent, function (req, res, next) {
    const id = req.params.id;
    jobFunctionController.getDetail(id, function (result) {
        res.send(result);
    });
});
module.exports = router;