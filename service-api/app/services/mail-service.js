var Mail = require('../models/mail-model');

var callbackData = { data: {}, message: "", error: "" };

module.exports = {
    insert: insert,
    update: update,
    updateRead: updateRead,
    updateMany: updateMany,
    deleted: deleted,
    getAll: getAll,
    getDetail: getDetail,
    findAll,
    findOneRecord
};

function insert(mail, callback) {
    try {
        mail.save().then((data) => {
            if (data.id) {
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
            } else {
                callbackData.data = { length: 0 };
                callbackData.message = "Failure";
            }
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = { length: 0 };
            callbackData.message = err.message;
            callbackData.error = err;
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function update(query, model, callback) {
    try {
        Mail.updateOne(query, model,{ upsert: true }).exec((err, result) => {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            if (result.n > 0) {
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
                callbackData.error = "";
            } else {
                callbackData.data = { length: 0 };
                callbackData.message = "The record does not exist";
                callbackData.error = "";
            }
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function updateRead(query, model, callback) {
    try {
        Mail.updateOne(query, model, { multi: true }).exec((err, result) => {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            if (result.n > 0) {
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
                callbackData.error = "";
            } else {
                callbackData.data = { length: 0 };
                callbackData.message = "The record does not exist";
                callbackData.error = "";
            }
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function updateMany(query, model, callback) {
    try {
        Mail.updateMany(query, model, function (err, result) {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err;
                return callback(callbackData);
            }
            if (result.n > 0) {
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
            } else {
                callbackData.data = { length: 0 };
                callbackData.message = "The record does not exist";
            }

            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function deleted(query, callback) {
    try {
        Mail.deleteOne(query, function (err, result) {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err;
                return callback(callbackData);
            }
            if (result.n > 0) {
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
                callbackData.error = "";
            } else {
                callbackData.data = { length: 0 };
                callbackData.message = "The record does not exist";
                callbackData.error = "";
            }
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function getAll(query, projections, sort, populate, callback) {
    try {
        Mail.find(query, projections).sort(sort).populate(populate).then((result) => {
            callbackData.data = { result: result };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = {};
            callbackData.message = err.message;
            callbackData.error = err.errors;
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err.errors;
        return callback(callbackData);
    }
};
function getDetail(query, projections, populate, callback) {
    try {
        Mail.findOne(query, projections).populate(populate).exec(function (err, result) {
            if (err) {
                callbackData.data = {};
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            callbackData.data = { result: result };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err.errors;
        return callback(callbackData);
    }
};

async function findAll(query) {
    try {
        return Mail.find(query);
    } catch (err) {
        return err;
    }
}

async function findOneRecord(query) {
    try {
        return Mail.findOne(query);
    } catch (err) {
        return err;
    }
}