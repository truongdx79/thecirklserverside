var Events = require('../models/event-model');

var callbackData = { data: {}, message: "", error: "" };

module.exports = {
    insert: insert,
    update: update,
    deleted: deleted,
    getAll: getAll,
    getDetail: getDetail,
    filterDate
};

function insert(events, callback) {
    try {
        events.save().then((data) => {
            callbackData.data = { length: 1 };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = { length: 0 };
            callbackData.message = err.message;
            callbackData.error = err;
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function update(query, model, callback) {
    try {
        Events.update(query, model, { upsert: true }, (err, result) => {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err;
                return callback(callbackData);
            }
            callbackData.data = { length: 1 };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function deleted(query, callback) {
    try {
        Events.findOneAndDelete(query,
            (err, result) => {
                if (err) {
                    callbackData.data = { length: 0 };
                    callbackData.message = err.message;
                    callbackData.error = err;
                    return callback(callbackData);
                }
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
                callbackData.error = "";
                return callback(callbackData);
            });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function getAll(query, projections, sort, populates, callback) {
    try {
        Events.find(query).select(projections).sort(sort).populate(populates).then((result) => {
            callbackData.data = { result: result };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = {};
            callbackData.message = err.message;
            callbackData.error = err.errors;
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err.errors;
        return callback(callbackData);
    }
};
function getDetail(query, projections, populates, callback) {
    try {

        Events.findOne(query, projections).populate(populates).exec(function (err, result) {
            if (err) {
                callbackData.data = {};
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            callbackData.data = { result: result };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err.errors;
        return callback(callbackData);
    }
};

async function filterDate(query) {
    try {
        return Events.find({ $where: function () { return (this.fromDate.getMonth() == query.month && this.fromDate.getYear() == query.year) } });
    } catch (err) {
        return callback({ error: err });
    }
}

