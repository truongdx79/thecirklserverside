var Jobs = require('../models/job-model');

var callbackData = { data: {}, message: "", error: "" };

module.exports = {
    insert: insert,
    update: update,
    deleted: deleted,
    getAll: getAll,
    getDetail: getDetail,
    findOneRecord
};

function insert(job, callback) {
    try {
        job.save().then((data) => {
            callbackData.data = { length: 1 };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = { length: 0 };
            callbackData.message = err.message;
            callbackData.error = err;
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};
function update(query, model, callback) {
    try {
        Jobs.updateOne(query, model, { upsert: true }, (err, result) => {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err;
                return callback(callbackData);
            }
            callbackData.data = { length: 1 };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};
function deleted(query, callback) {
    try {
        Jobs.findOneAndDelete(query,
            (err, result) => {
                if (err) {
                    callbackData.data = { length: 0 };
                    callbackData.message = err.message;
                    callbackData.error = err;
                    return callback(callbackData);
                }
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
                callbackData.error = "";
                return callback(callbackData);
            });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};
function getAll(query, projections, sort, populates, callback) {
    try {
        Jobs.find(query, projections).sort(sort).populate(populates).then((result) => {
            callbackData.data = result;
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = {};
            callbackData.message = err.message;
            callbackData.error = err.errors;
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err.errors;
        return callback(callbackData);
    }
};
function getDetail(query, projections, populates, callback) {
    try {
        Jobs.findOne(query, projections).populate(populates).exec(function (err, result) {
            if (err) {
                callbackData.data = {};
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            callbackData.data = result;
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err.errors;
        return callback(callbackData);
    }
};
async function findOneRecord(query) {
    try {
        return Jobs.findOne(query);
    } catch (err) {
        return err;
    }
}
