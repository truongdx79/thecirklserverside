const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const configs = require("../../config");
var Users = require('../models/user-model');

var callbackData = { data: {}, message: "", error: "" };

module.exports = {
    login: login,
    adminLogin: adminLogin,
    insert: insert,
    update: update,
    deleted: deleted,
    getAll: getAll,
    findByAggregate: findByAggregate,
    findByDistinct: findByDistinct,
    getDetail: getDetail,
    checkExistLinkedinId,
    fildUserById
};

function login(query, callback) {
    try {
        Users.findOne(query, function (err, result) {
            if (err) {
                callbackData.data = {};
                callbackData.message = err.message;
                callbackData.error = err;
                return callback(callbackData);
            }
            if (!result) {
                callbackData.data = {};
                callbackData.message = 'Authentication failed. User not found!';
                callbackData.error = "";
                return callback(callbackData);
            } else {
                if (result.isActive) {
                    var token = jwt.sign(({ id: result._id, linkedinId: result.linkedinId, userName: result.userName }), configs.secret);
                    callbackData.data = { token: token };
                    callbackData.message = 'Success';
                    callbackData.error = "";
                    return callback(callbackData);
                } else {
                    callbackData.data = {};
                    callbackData.message = 'Authentication failed. User does not have access!';
                    callbackData.error = "";
                    return callback(callbackData);
                }

            }
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
}

function adminLogin(query, password, callback) {
    try {
        Users.findOne(query, function (err, result) {
            if (err) {
                callbackData.data = { auth: false };
                callbackData.message = err.message;
                callbackData.error = err;
                return callback(callbackData);
            }
            if (!result) {
                callbackData.data = { auth: false };
                callbackData.message = 'Authentication failed. User not found!';
                callbackData.error = "";
                return callback(callbackData);
            } else {
                if (result.password !== String(password)) {
                    callbackData.data = { auth: false, token: "" };
                    callbackData.message = 'Authentication failed. Wrong password.';
                    callbackData.error = '';
                    return callback(callbackData);
                } else {
                    if (result.isActive) {
                        var token = jwt.sign(({ id: result._id, firstName: result.firstName, lastName: result.firstName }), configs.secret);
                        callbackData.data = { auth: true, token: token, role: result.role };
                        callbackData.message = 'Success';
                        callbackData.error = "";
                        return callback(callbackData);
                    } else {
                        callbackData.data = { auth: false };
                        callbackData.message = 'Authentication failed. User does not have access!';
                        callbackData.error = "";
                        return callback(callbackData);
                    }
                }

            }
        });
    } catch (err) {
        callbackData.data = { auth: false };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
}

function insert(user, callback) {
    try {
        user.save().then((data) => {
            callbackData.data = { length: 1 };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = { length: 0 };
            callbackData.message = err.message;
            callbackData.error = err.errors;
            return callback(callbackData);

        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function update(query, user, callback) {
    try {
        // Users.findById(query, function (err, u) {
        //     if (err) {
        //         callbackData.data = { length: 0 };
        //         callbackData.message = err.message;
        //         callbackData.error = err.errors;
        //         return callback(callbackData);
        //     }
        //     u.set(user);
        //     u.save(function (err, result) {
        //         if (err) {
        //             callbackData.data = { length: 0 };
        //             callbackData.message = err.message;
        //             callbackData.error = err.errors;
        //             return callback(callbackData);
        //         }
        //         callbackData.data = { length: 1 };
        //         callbackData.message = "Success";
        //         callbackData.error = "";
        //         return callback(callbackData);
        //     });
        // });
        Users.updateOne(query, user, { upsert: true }).exec((err, result) => {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            if (result.n > 0) {
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
                callbackData.error = "";
            } else {
                callbackData.data = { length: 0 };
                callbackData.message = "Failure";
                callbackData.error = "";
            }
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function deleted(query, callback) {
    try {
        Users.findOneAndDelete(query, (err, result) => {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            if (result.n > 0) {
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
                callbackData.error = "";
            } else {
                callbackData.data = { length: 0 };
                callbackData.message = "Failure";
                callbackData.error = "";
            }
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function getAll(query, projections, sorts, populate, callback) {
    try {
        var query = Users.find(query);
        query.select(projections);
        query.sort(sorts);
        query.populate(populate);
        query.exec(function (err, result) {
            if (err) {
                callbackData.data = {};
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            callbackData.data = { result: result };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function findByAggregate(query, callback) {
    try {
        Users.aggregate(query, function (err, result) {
            if (err) {
                callbackData.data = {};
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            callbackData.data = { result: result };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        })
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function findByDistinct(field, query, callback) {
    try {
        Users.distinct(field, query, function (err, result) {
            if (err) {
                callbackData.data = {};
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            callbackData.data = { result: result.sort() };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        })
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function getDetail(query, projections, populates, callback) {
    try {

        Users.findOne(query, projections).populate(populates)
            .exec(function (err, result) {
                if (err) {
                    callbackData.data = {};
                    callbackData.message = err.message;
                    callbackData.error = err.errors;
                    return callback(callbackData);
                }
                callbackData.data = { result: result };
                callbackData.message = "Success";
                callbackData.error = "";
                return callback(callbackData);
            });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

async function checkExistLinkedinId(query) {
    try {
        return Users.countDocuments(query);
    } catch (err) {
        return { error: err } //error
    }
};

async function fildUserById(query) {
    try {
        return Users.findOne(query);
    } catch (err) {
        return { error: err }
    }
}