var JobFunction = require('../models/job-function-model');

var callbackData = { data: {}, message: "", error: "" };

module.exports = {
    insert: insert,
    update: update,
    deleted: deleted,
    getAll: getAll,
    getDetail: getDetail
};

function insert(jobFunction, callback) {
    try {
        jobFunction.save().then((data) => {
            callbackData.data = { length: 1 };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = { length: 0 };
            callbackData.message = err.message;
            callbackData.error = err;
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function update(query, model, callback) {
    try {
        JobFunction.findOneAndUpdate(query, model, { upsert: true }, (err, result) => {
            if (err) {
                callbackData.data = { length: 0 };
                callbackData.message = err.message;
                callbackData.error = err;
                return callback(callbackData);
            }
            callbackData.data = { length: 1 };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function deleted(query, callback) {
    try {
        JobFunction.findOneAndDelete(query,
            (err, result) => {
                if (err) {
                    callbackData.data = { length: 0 };
                    callbackData.message = err.message;
                    callbackData.error = err;
                    return callback(callbackData);
                }
                callbackData.data = { length: 1 };
                callbackData.message = "Success";
                callbackData.error = "";
                return callback(callbackData);
            });
    } catch (err) {
        callbackData.data = { length: 0 };
        callbackData.message = err.message;
        callbackData.error = err;
        return callback(callbackData);
    }
};

function getAll(query,projections,sort, callback) {
    try {
        JobFunction.find(query, projections).sort(sort).then((result) => {
            callbackData.data = { result: result };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        }, (err) => {
            callbackData.data = {};
            callbackData.message = err.message;
            callbackData.error = err.errors;
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err.errors;
        return callback(callbackData);
    }
};
function getDetail(query, projections, callback) {
    try {

        JobFunction.findOne(query, projections, function (err, result) {
            if (err) {
                callbackData.data = {};
                callbackData.message = err.message;
                callbackData.error = err.errors;
                return callback(callbackData);
            }
            callbackData.data = { result: result };
            callbackData.message = "Success";
            callbackData.error = "";
            return callback(callbackData);
        });
    } catch (err) {
        callbackData.data = {};
        callbackData.message = err.message;
        callbackData.error = err.errors;
        return callback(callbackData);
    }
};