const mongoose = require('mongoose');

//Định nghĩa một schema
var Schema = mongoose.Schema;
const industrySchema = new Schema({
    name: { type: String, trim: true, required: true },
    _createDate: { type: Date, default: Date.now() },
    _updateDate: { type: Date, default: Date.now() }
}, { versionKey: false });
industrySchema.set('collection', 'industry');
// Biên dịch mô hình từ schema
var industry = mongoose.model('industry', industrySchema);

module.exports = industry;


// db.industry.insert([{
//     "name":"Accounting",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Airlines/Aviation",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Alternative Dispute Resolution",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Alternative Medicine",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Animation",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Apparel & Fashion",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Architecture & Planning",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Arts and Crafts",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Automotive",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Aviation & Aerospace",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Banking",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Biotechnology",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Broadcast Media",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Building Materials",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Business Supplies and Equipment",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Capital Markets",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Chemicals",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Civic & Social Organization",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Civil Engineering",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Commercial Real Estate",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Computer & Network Security",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Computer Games",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Computer Hardware",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Computer Networking",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Computer Software",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Construction",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Consumer Electronics",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Consumer Goods",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Consumer Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Cosmetics",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Dairy",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Defense & Space",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Design",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Education Management",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"E-Learning",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Electrical/Electronic Manufacturing",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Entertainment",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Environmental Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Events Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Executive Office",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Facilities Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Farming",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Financial Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Fine Art",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Fishery",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Food & Beverages",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Food Production",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Fund-Raising",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Furniture",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Gambling & Casinos",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Glass, Ceramics & Concrete",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Government Administration",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Government Relations",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Graphic Design",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Health, Wellness and Fitness",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Higher Education",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Hospital & Health Care",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Hospitality",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Human Resources",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Import and Export",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Individual & Family Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Industrial Automation",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Information Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Information Technology and Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Insurance",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"International Affairs",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"International Trade and Development",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Internet",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Investment Banking",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Investment Management",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Judiciary",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Law Enforcement",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Law Practice",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Legal Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Legislative Office",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Leisure, Travel & Tourism",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Libraries",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Logistics and Supply Chain",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Luxury Goods & Jewelry",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Machinery",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Management Consulting",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Maritime",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Market Research",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Advertising",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Marketing",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Consulting",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Travel",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Mechanical or Industrial Engineering",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Media Production",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Medical Devices",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Medical Practice",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Mental Health Care",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Military",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Mining & Metals",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Motion Pictures and Film",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Museums and Institutions",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Music",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Nanotechnology",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Newspapers",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Non-Profit Organization Management",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Oil & Energy",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Online Media",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Outsourcing/Offshoring",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Package/Freight Delivery",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Packaging and Containers",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Paper & Forest Products",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Performing Arts",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Pharmaceuticals",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Philanthropy",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Photography",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Plastics",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Political Organization",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Primary/Secondary Education",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Printing",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Professional Training & Coaching",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Program Development",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Public Policy",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Public Relations and Communications",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Public Safety",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Publishing",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Railroad Manufacture",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Ranching",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Real Estate",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Recreational Facilities and Services",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Religious Institutions",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Renewables & Environment",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Research",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Restaurants",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Retail",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Security and Investigations",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Semiconductors",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Shipbuilding",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Sporting Goods",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Sports",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Staffing and Recruiting",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Supermarkets",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Telecommunications",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Textiles",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Think Tanks",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Tobacco",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Translation and Localization",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Transportation/Trucking/Railroad",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Utilities",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Venture Capital & Private Equity",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Veterinary",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Warehousing",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Wholesale",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Wine and Spirits",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Wireless",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// },{
//     "name":"Writing and Editing",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }
// ])