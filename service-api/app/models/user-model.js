const interest = require('../models/interest-model');
const skill = require('../models/skill-model');
const industry = require('../models/industry-model');
const mongoose = require('mongoose');

let strRegexVn = /^[a-zA-Z0-9 `'.,-_/ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]*$/;
let strRegexEn = /^[a-zA-Z0-9]*$/;
let strPhone = /^\(?\+?[0-9]{1,3}\)? ?-?[0-9]{1,3}? ?-?[0-9]{3,5}? ?-?[0-9]{4}?$/;

//Định nghĩa một schema
var Schema = mongoose.Schema;
const UserSchema = new Schema({
    firstName: {
        type: String, trim: true, required: [true, 'FurstName is required']
    },
    lastName: {
        type: String, trim: true, required: [true, 'LastName is required']
    },
    fullName: { type: String, trim: true, default: "" },
    company: { type: String, trim: true, default: "" },
    headline: { type: String, trim: true, default: "" },
    avatarUrl: { type: String, trim: true, default: "" },
    currentPosition: { type: String, trim: true, default: "" },
    location: { type: String, trim: true, default: "" },
    long: { type: Number, default: 0 },
    lat: { type: Number, default: 0 },
    available: { type: Boolean, default: true },
    emailAddress: { type: String, trim: true },
    phone: { type: String, trim: true, maxlength: 20, default: "" },
    introduction: { type: String, trim: true },
    interests: [{ type: Schema.Types.ObjectId, ref: 'interest' }],
    educations: [
        {
            _id: { type: Schema.Types.ObjectId },
            school: { type: String, trim: true },
            degree: { type: String, trim: true, default: "" },
            fieldOfStudy: { type: String, trim: true, default: "" },
            graduationYear: { type: String, trim: true, default: "" },
        }
    ],
    socials: [{
        _id: { type: Schema.Types.ObjectId },
        type: { type: String },
        name: { type: String }
    }],
    careers: [{
        _id: { type: Schema.Types.ObjectId },
        company: { type: String, trim: true, default: "" },
        industry: { type: Schema.Types.ObjectId, ref: 'industry' },
        skills: [{ type: Schema.Types.ObjectId, ref: 'skill' }],
        position: { type: String, trim: true, default: "" },
        level: { type: String, trim: true, default: "" },
        current: { type: Boolean, default: true },
        fromDate: { type: Date },
        toDate: { type: Date }
    }],
    linkedinId: {
        type: String, index: true, unique: true, required: [true, 'linkedinId is required']
    },
    isActive: { type: Boolean, default: true },
    role: { type: String, default: "" },
    password: { type: String, trim: true, default: "" },
    _createDate: { type: Date, default: Date.now },
    _updateDate: { type: Date, default: Date.now }
}, { versionKey: false });
UserSchema.set('collection', 'user');

// Biên dịch mô hình từ schema
var User = mongoose.model('user', UserSchema);
module.exports = User;

// db.getCollection("user").insert({
//     "company": "TheCirkl",
//     "headline": "User system management",
//     "avatarUrl": "",
//     "currentPosition": "System management",
//     "location": "Eton Tower Podium, Causeway Bay, Hong Kong",
//     "long": 114.18485663242227,
//     "lat": 22.278152379811896,
//     "available": true,
//     "phone": "",
//     "interests": [

//     ],
//     "isActive": true,
//     "role": "system",
//     "password": "123thecirkl",
//     "linkedinId": "system123",
//     "firstName": "User",
//     "lastName": "System",
//     "emailAddress": "admin@thecirkl.com",
//     "introduction": "The user management system",
//     "educations": [
//         {
//             "degree": "Executive MBa",
//             "fieldOfStudy": "Business",
//             "graduationYear": "2019",
//             "_id": ObjectId("5c93a93c0c968d4bc519cb17"),
//             "school": "Kellogg-HKUST"
//         }
//     ],
//     "careers": [
//         {
//             "company": "Roche",
//             "skills": [],
//             "position": "Director",
//             "level": "Director",
//             "current": false,
//             "_id": ObjectId("5cb033130c698a5fd77404c5"),
//             "category": ObjectId("5cae5acea0d09722484fbceb"),
//             "fromDate": ISODate("2019-04-12T13:38:23.232+07:00"),
//             "toDate": ISODate("2019-05-12T13:38:23.232+07:00")
//         }
//     ],
//     "socials": [],
//     "_createDate": ISODate("2019-04-12T13:41:23.466+07:00"),
//     "_updateDate": ISODate("2019-04-12T13:41:23.466+07:00")
// })

// schools: [
//     {
//       "_id": "5c93a93c0c968d4bc519cb17",
//       "name": "Kellogg-HKUST"
//     },
//     {
//       "_id": "5c93a93c0c968d4bc519cb18",
//       "name": "Kellogg"
//     },
//     {
//       "_id": "5c944a9e0c968d4bc519cb19",
//       "name": "Kellogg-Schulich"
//     },
//     {
//       "_id": "5c944a9e0c968d4bc519cb20",
//       "name": "Kellogg-Recanati"
//     },
//     {
//       "_id": "5c944a9e0c968d4bc519cb21",
//       "name": "Kellogg-WHU"
//     },
//     {
//       "_id": "5c944a9e0c968d4bc519cb22",
//       "name": "Kellogg-Guanghua"
//     }
//   ],