const user = require('../models/user-model');
const mongoose = require('mongoose');

//Định nghĩa một schema
var Schema = mongoose.Schema;
const interestSchema = new Schema({
    parentId: { type: Schema.Types.ObjectId },
    userFrom: { type: Schema.Types.ObjectId, ref: 'user' },
    userTo: [{ type: Schema.Types.ObjectId, ref: 'user' }],
    title: { type: String, trim: true, required: true },
    description: { type: String, required: true },
    read: [{ type: String }],
    isTrash: { type: Boolean, default: false },
    reply: { type: Boolean, default: false },
    _createDate: { type: Date, default: Date.now() }
}, { versionKey: false });
interestSchema.set('collection', 'mail');
// Biên dịch mô hình từ schema
var interest = mongoose.model('mail', interestSchema);

module.exports = interest;