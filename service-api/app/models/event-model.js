const user = require('../models/user-model');
const interest = require('../models/interest-model');
const mongoose = require('mongoose');

//Định nghĩa một schema
var Schema = mongoose.Schema;
const EventSchema = new Schema({
    name: { type: String, trim: true, required: true },
    picture: { type: String, trim: true, default: "" },
    location: { type: String, trim: true, default: "" },
    long: { type: Number, default: 0 },
    lat: { type: Number, default: 0 },
    totalDay: { type: String, default: "" },
    fromDate: { type: Date, required: true },
    toDate: { type: Date, required: true },
    fromTime: { type: Date, required: true },
    toTime: { type: Date, required: true },
    description: { type: String, trim: true, required: true },
    activity: { type: Schema.Types.ObjectId, required: true, ref: 'interest' }, //join to interest
    owner: { type: Schema.Types.ObjectId, required: true, ref: 'user' },//user create event
    participate: [{ type: Schema.Types.ObjectId, required: true, ref: 'user' }],//user join event
    restrictions: [ //han che nguoi tham gia bang school
        {
            _id: { type: Schema.Types.ObjectId },
            name: { type: String, trim: true }
        }
    ],
    limitNumber:{ type: Number, default: 0 },//gio han so nguoi tham gia
    _createDate: { type: Date, default: Date.now() },
    _updateDate: { type: Date, default: Date.now() }
}, { versionKey: false });
EventSchema.set('collection', 'events');
// Biên dịch mô hình từ schema
var Events = mongoose.model('events', EventSchema);

module.exports = Events;