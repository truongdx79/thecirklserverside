const mongoose = require('mongoose');

//Định nghĩa một schema
var Schema = mongoose.Schema;
const interestSchema = new Schema({
    name: { type: String, trim: true, required: true },
    icon: { type: String, required: true,default: "" },
    _createDate: { type: Date, default: Date.now() },
    _updateDate: { type: Date, default: Date.now() }
}, { versionKey: false });
interestSchema.set('collection', 'interest');
// Biên dịch mô hình từ schema
var Interest = mongoose.model('interest', interestSchema);

module.exports = Interest;

// db.interest.insert([
//     {
//     "name":"Architecture",
//     "icon":"Architecture.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Art-exhibitions",
//     "icon":"Art-exhibitions.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Badminton",
//     "icon":"Badminton.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Ballet",
//     "icon":"Ballet.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Bowling",
//     "icon":"Bowling.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Chess",
//     "icon":"Chess.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Cocktails",
//     "icon":"Cocktails.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Coffee drinking",
//     "icon":"coffee-drinking.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Craft beer",
//     "icon":"Craft-beer.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Cycling",
//     "icon":"Cycling.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Fine dining",
//     "icon":"Fine-dining.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Fishing",
//     "icon":"Fishing.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Golf",
//     "icon":"Golf.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Hiking",
//     "icon":"Hiking.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Horse riding",
//     "icon":"Horse-riding.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Live musical",
//     "icon":"Live-musical.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Live sport",
//     "icon":"Live-sport.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Movies",
//     "icon":"Movies.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Opera",
//     "icon":"Opera.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Others",
//     "icon":"Others.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Photography",
//     "icon":"Photography.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Pilates",
//     "icon":"Pilates.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Poker",
//     "icon":"Poker.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Polo",
//     "icon":"Polo.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Rowing",
//     "icon":"Rowing.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Running",
//     "icon":"Running.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Sailing",
//     "icon":"Sailing.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Singing karaoke",
//     "icon":"Singing-karaoke.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Skating",
//     "icon":"Skating.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Skiing",
//     "icon":"Skiing.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Snooker",
//     "icon":"Snooker.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Snorkeling",
//     "icon":"Snorkeling.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Snowboarding",
//     "icon":"Snowboarding.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Squash",
//     "icon":"Squash.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Street food",
//     "icon":"Street-food.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Surfing",
//     "icon":"Surfing.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Tea drinking",
//     "icon":"Tea drinking.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Tennis",
//     "icon":"Tennis.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Theater",
//     "icon":"Theater.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Water-skiing",
//     "icon":"Water-skiing.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Weight-training",
//     "icon":"Weight-training.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Whiskey",
//     "icon":"Whiskey.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Wine tasting",
//     "icon":"Wine-tasting.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }, {
//     "name":"Yoga",
//     "icon":"Yoga.png",
//    "_createDate": ISODate("2019-03-27T13:48:19.215+07:00"),
//         "_updateDate": ISODate("2019-03-27T13:48:19.215+07:00")
// }
//     ]) 