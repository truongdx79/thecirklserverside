const industry = require('../models/industry-model');
const skill = require('../models/skill-model');
const user = require('../models/user-model');
const mongoose = require('mongoose');

//Định nghĩa một schema
var Schema = mongoose.Schema;
const jobSchema = new Schema({
    classification: { type: String, trim: true, required: true, enum: ['Jobseeker', 'Recruiter'] },
    jobType: { type: String, trim: true, required: true },
    title: { type: String, trim: true, required: true },
    location: { type: String, trim: true, default: "" },
    long: { type: Number, default: 0 },
    lat: { type: Number, default: 0 },
    industry: { type: Schema.Types.ObjectId, ref: 'industry' },
    skills: [{ type: Schema.Types.ObjectId, ref: 'skill' }],
    owner: { type: Schema.Types.ObjectId, ref: 'user' },
    accept: [{ type: Schema.Types.ObjectId, ref: 'user' }],
    experience: { type: String },
    salary: { type: String, default: '0' },
    currency: { type: String },
    salaryType: { type: String },
    startDate: { type: Date, default: Date.now() },
    expirationDate: { type: Date, default: Date.now() },
    expired: { type: Boolean },
    read: [{ type: String }],
    totalDay: { type: String, default: "" },
    description: { type: String, required: true },
    isTrash: { type: Boolean, default: false },
    _createDate: { type: Date, default: Date.now() },
    _updateDate: { type: Date, default: Date.now() }
}, { versionKey: false });
jobSchema.set('collection', 'job');
// Biên dịch mô hình từ schema
var job = mongoose.model('job', jobSchema);

module.exports = job;