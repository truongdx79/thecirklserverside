import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent } from '@angular/common/http';
import { APP_CONFIG } from 'src/app/configs/app-config.service';
import * as _ from 'underscore';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private httpOptions: any;
  private API_URL: string;

  constructor(private httpService: HttpClient) {
    this.API_URL = `${APP_CONFIG.DNSRoot}`;

    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
  }
  login(user: any) {
    return this.httpService.post(`${this.API_URL}/admin/login`, user, this.httpOptions);
  }

  
  updateActive(user: any) {
    return this.httpService.put(`${this.API_URL}/user/active`, user, this.httpOptions);
  }
 
  getAll() {
    return this.httpService.get(`${this.API_URL}/user/all`, this.httpOptions);
  }

  getDetail(id: any) {
    return this.httpService.get(`${this.API_URL}/user/detail/${id}`, this.httpOptions);
  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);
    let startPage: number, endPage: number;
    if (totalPages <= 5) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 3) {
        startPage = 1;
        endPage = 5;
      } else if (currentPage + 1 >= totalPages) {
        startPage = totalPages - 4;
        endPage = totalPages;
      } else {
        startPage = currentPage - 2;
        endPage = currentPage + 2;
      }
    }
    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
    // create an array of pages to ng-repeat in the pager control
    let pages = _.range(startPage, endPage + 1);
    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }

}
