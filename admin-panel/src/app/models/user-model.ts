export class User {
    _id: String;
    firstName: String;
    lastName: String;
    company: String;
    headline: String;
    avatarUrl: String;
    currentPosition: String;
    location: String;
    long: Number;
    lat: Number;
    available: Boolean;
    emailAddress: String;
    phone: String;
    name: String;
    introduction: String;
    interests: [{
        _id: String;
        name: String;
    }];
    educations: [
        {
            _id: String;
            school: String;
            degree: String;
            fieldOfStudy: String;
            graduationYear: String;
        }
    ];
    socials: [{
        _id: String;
        type: String;
        name: String;
    }];
    careers: [{
        _id: String;
        company: String;
        category: {
            _id: String;
            name: String;
        };
        skills: [{
            _id: String;
            name: String;
        }];
        level: String;
        fromDate: Date;
        toDate: Date;
        linkedinId: String;
        isActive: Boolean;
        role: String;
        password: String;
        _createDate: Date;
        _updateDate: Date;

    }]
}