import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../service/authorization/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService,
    private router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const authToken = this.auth.getToken();
      if (this.auth.isLoggednIn()) {
        return true;
      }
      // sessionStorage.setItem('ExpedystCurrentUrl', location.href);
      this.router.navigate(['admin','login']);
      return false;
  }
}
