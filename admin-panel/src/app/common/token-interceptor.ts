import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Route } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError } from "rxjs/operators";

export class TokenInterceptor  {
    constructor() { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.includes('config.json')) {
            return next.handle(req);
        }
        const authToken = localStorage.getItem('LoggedInUser');
        let authReq: HttpRequest<any>;
        let forceRedirectOnFailure = false;

        if (authToken === null) {
            authReq = req.clone();
        } else {
            authReq = req.clone({ headers: req.headers.set('Authorization', localStorage.getItem('LoggedInUser')) });
            forceRedirectOnFailure = true;
        }

        return next.handle(authReq).pipe(
            catchError(err => {
                // console.log(err);
                if ((err.status === 401) && (forceRedirectOnFailure)) {
                    localStorage.clear();
                    sessionStorage.clear();
                    this.redirectToLogin();
                }
                return Observable.throw(err);
            })
        ) as any;
    }

    redirectToLogin() {
        // sessionStorage.setItem('RestaurantCurrentUrl', location.href);
        location.href = '/admin/login';
        
    }
}
