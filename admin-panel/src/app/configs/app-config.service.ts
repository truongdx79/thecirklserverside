import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class AppConfig {
  readonly DNSRoot: string;
  readonly DNSRootLogin: string;
  readonly SocketRoot: string;
}

export let APP_CONFIG: AppConfig;

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  constructor(private http: HttpClient) {
  }

  public load() {
      return new Promise((resolve, reject) => {
          this.http.get('/assets/json/config.json').subscribe((envResponse: any) => {
              const t = new AppConfig();
              APP_CONFIG = Object.assign(t, envResponse);
              resolve(true);
          });
      });
  }
}
