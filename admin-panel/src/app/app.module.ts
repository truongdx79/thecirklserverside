import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { routing } from '../router.config';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LoadingModule } from 'ngx-loading';
import { FileUploadModule } from 'ng2-file-upload';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AppConfigService } from './configs/app-config.service';
import { AppComponent } from './app.component';
import { TokenInterceptor } from './common/token-interceptor';
import { SidebarLeftComponent } from './component/sidebar-left/sidebar-left.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { NotfoundComponent } from './component/notfound/notfound.component';
import { HeaderComponent } from './component/header/header.component';
import { LoginComponent } from './component/login/login.component';

export function loadConfigService(configService: AppConfigService): Function {
  return () => configService.load();
}

@NgModule({
  declarations: [
    AppComponent,
    SidebarLeftComponent,
    DashboardComponent,
    NotfoundComponent,
    HeaderComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    routing,
    NgbModule.forRoot(),
    TagInputModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    LoadingModule,
    NgMultiSelectDropDownModule.forRoot(),
    FileUploadModule
  ],
  providers: [
    AppConfigService, {
      provide: APP_INITIALIZER,
      useFactory: loadConfigService,
      deps: [AppConfigService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
