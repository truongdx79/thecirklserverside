import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { UserService } from '../../service/apis/user.service';
import { LoginInfo } from '../../models/login';
import { Router } from '@angular/router';
import { AuthService } from '../../service/authorization/auth.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginInfo: LoginInfo = new LoginInfo();

  constructor(private userService: UserService,private auth: AuthService, private router:Router) {
  }

  ngOnInit() {
    if (this.auth.isLoggednIn()) {
      return window.location.href = '/';
    }
    $('.u-side-nav-mini').addClass('bg-page-login');
    $('app-header').remove();
    $('app-sidebar-left').remove();
    $('#sideNav').remove();
    $('.container-fluid').removeClass('g-pt-65');
    $('#main-body').removeClass('g-pa-20');
    // $('.wrapper-main').addClass('wrapper-login');
  }

  login() {
    event.preventDefault();
    sessionStorage.setItem('ThedirklNotRequireToken', 'login');
    this.userService.login(this.loginInfo).subscribe((result: any) => {
      if (result.data.auth) {
        this.auth.sendToken(result.data.token)
        localStorage.setItem('ThedirklUserEntityName', 'IOTech Co. Ltd');
        if (result.data.role === 'system') {
          
          return window.location.href = '/';
        }
      }
      else {
        swal('Error', result.message, 'warning');
      }

    }, (err) => {
      // this.hideBody = true;
      swal('Error', `Connection server error`, 'error');
    });
  }

}
