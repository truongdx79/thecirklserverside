import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/apis/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import swal from 'sweetalert2';
import { User } from '../../models/user-model';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  // array of all items to be paged
  public rows = [];
  public userInfo: User;
  private selectedItemDelete: any;
  public selectedName: any;
  public imageUrl = '';
  public socials = [];
  public educations = [];
  public careers = [];
  public interests = [];

  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];

  constructor(private userApiService: UserService) {
  }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.userApiService.getAll().subscribe((result: any) => {
      this.rows = result.data.result;
      this.setPage(1);
    });
  };

  changeActive(event, row, index) {
    this.userApiService.updateActive({ _id: row._id, isActive: !row.isActive }).subscribe((result: any) => {
      if (result.data.length > 0) {
        this.rows[index].isActive = !row.isActive;
        swal('Success', `You have change isActive successfully.`, 'success');
      } else {
        this.rows[index].isActive = row.isActive;
        swal('Error', result.message, 'error');
      }
    }, (err: HttpErrorResponse) => {
      this.rows[index].isActive = row.isActive;
      swal('Error', 'Server error occurred.', 'error');
    });
  }

  selectedDettail(row, index) {
    this.userApiService.getDetail(row._id).subscribe((result: any) => {
      if (result.data.result) {
        $('#confirmDetailApp').modal('toggle');
        this.userInfo = result.data.result;
        if (result.data.result.avatarUrl) {
          this.imageUrl = result.data.result.avatarUrl;
        }
        this.socials = result.data.result.socials;
        this.educations = result.data.result.educations;
        this.careers = result.data.result.careers;
        console.log(this.socials);
        
        this.interests = result.data.result.interests;
      }
    }, (err: HttpErrorResponse) => {
      this.rows[index].isActive = row.isActive;
      swal('Error', 'Server error occurred.', 'error');
    });
  }


  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    // get pager object from service
    this.pager = this.userApiService.getPager(this.rows.length, page);
    // get current page of items
    this.pagedItems = this.rows.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
