import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-notfound',
  templateUrl: './notfound.component.html',
  styleUrls: ['./notfound.component.css']
})
export class NotfoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('app-header').remove();
    $('app-sidebar-left').remove();
    $('#sideNav').remove();
    $('.container-fluid').removeClass('g-pt-65');
    $('#main-body').removeClass('g-pa-20');
  }

}
