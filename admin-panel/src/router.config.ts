import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './app/component/dashboard/dashboard.component';
import { NotfoundComponent } from './app/component/notfound/notfound.component';
import { AuthGuard } from './app/common/auth.guard';
import { LoginComponent } from './app/component/login/login.component';

const routerConfig: Routes = [
    {
        path: '',
        redirectTo: 'admin/dashboard',
        pathMatch: 'prefix',
        canActivate: [AuthGuard]
    },
    {
        path: 'admin/dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'admin/login',
        component: LoginComponent
    },
    { path: '404', component: NotfoundComponent },
    {
        path: '**',
        redirectTo: '/404',
        pathMatch: 'prefix'
    }
];

export const routing = RouterModule.forRoot(routerConfig);
// export const routing = RouterModule.forRoot(routerConfig, { useHash: true });